from PySide.QtCore import *
from PySide.QtGui import *


import sys
import os
import os.path
import logging.handlers
from _winreg import ConnectRegistry, OpenKey, EnumValue, HKEY_CURRENT_USER, QueryInfoKey, SetValueEx, KEY_ALL_ACCESS, REG_BINARY
from base64 import standard_b64decode, standard_b64encode
from collections import OrderedDict
import traceback

from editor import Ui_Editor
from libbtsave import ProtobufMessage, FlattenedArray, EWireType
from models import InventoryTableModel, InventoryProxyModel
from libbtmechporter import MechPorterSettingsStore, CostCalculator
from mechnamesetter import MechNameSetter
from arrayeditor import GenericArrayEditor
from libbtsave.utils.jsonreader import readJsonFile
from descriptioneditor import DescriptionEditor
from hardpointeditor import HardPointEditor
from actuatorlimiter import ActuatorLimiter
from assemblyvariant import AssemblyVariant
from chassisdefaulteditor import ChassisDefaultEditor
from armorstructeditor import ArmorStructEditor
import json

STR_VERSION = '201.2.4-20200528'

rootLogger = logging.getLogger('BTMechport')
rootLogger.setLevel(logging.INFO)
obj_handler = logging.FileHandler('BtMechporter.txt', mode='wb')
obj_formatter = logging.Formatter('%(levelname)-10s (%(module)-20s:%(funcName)-30s:%(lineno)-4d) %(message)s')
obj_handler.setFormatter(obj_formatter)
obj_handler.setLevel(logging.DEBUG)
rootLogger.addHandler(obj_handler)


class BattleTechMechPorter(QMainWindow, Ui_Editor):
    """
    Main Editor UI

    """

    SettingsFile = 'BtMechPorterSettings.xml'
    Signal_ScanDone = Signal(bool)
    StandardMech = 'Standard Mech'
    PirateMech = 'Pirate Mech'
    EliteMech = 'Elite Mech'
    JkHero = 'JK Hero Mech'
    ClanMech = 'Clan Mech'
    PrimativeMech = 'Primative Mech'
    PowerArmorMech = 'Power Armor'
    LocalMarker = '<LOCAL>'
    RtCache = 'RogueTechSaves' + os.sep + 'CachedSettings.pref'
    RtCloud = 'RogueTechSaves' + os.sep + 'cloud' + os.sep + 'C0' + os.sep + 'settings_cloud.sav'
    BtaCache = 'BTASaves' + os.sep + 'CachedSettings.pref'
    BtaCloud = 'BTASaves' + os.sep + 'cloud' + os.sep + 'C0' + os.sep + 'settings_cloud.sav'
    BtiCache = 'BTISaves' + os.sep + 'CachedSettings.pref'
    BtiCloud = 'BTISaves' + os.sep + 'cloud' + os.sep + 'C0' + os.sep + 'settings_cloud.sav'

    MultiplierCost = {
        StandardMech : CostCalculator.StandardMulitplier,
        PirateMech : CostCalculator.PirateMulitplier,
        EliteMech : CostCalculator.EliteMulitplier,
        JkHero : CostCalculator.JkHeroMulitplier,
        ClanMech : CostCalculator.ClannerMulitplier,
        PrimativeMech : CostCalculator.PrimativeMulitplier,
        PowerArmorMech : CostCalculator.PowerArmorMulitplier
    }

    def __init__(self, obj_qapp, parent=None):
        super(BattleTechMechPorter, self).__init__(parent)
        self.setupUi(self)
        self.qapp = obj_qapp
        self.setWindowTitle('BattleTech Mechporter v' + STR_VERSION)
        self.ProgressDialog = None
        self.MechValidationTags = []
        self.ChassisValidationTags = []
        self.LastLoadedData = ''
        self.LocalSettingsData = ''
        self.LoadReg = False
        self.LastLoadFile = ''
        self.CloudFile = ''
        self.MdSum = ''

        # self.groupBox.hide()

        self.Settings = MechPorterSettingsStore()
        if os.path.exists(self.SettingsFile):
            self.Settings.fromFile(self.SettingsFile)

        self.Settings.setFilePath(self.SettingsFile)

        self.checkBox_exportRtWikiData.setChecked(self.Settings.RtWikiExportForTim)
        self.checkBox_exportPrefabs.setChecked(self.Settings.ExportWeaponPrefabs)
        self.checkBox_removeUnitCustom.setChecked(self.Settings.RemoveUnitCustom)
        self.checkBox_addUnitRelease.setChecked(self.Settings.AddUnitRelease)
        self.checkBox_exportNewWeaponId.setChecked(self.Settings.UseWeaponId)
        self.checkBox_reorderDef.setChecked(self.Settings.ReOrderDefs)

        self.dataModel = InventoryTableModel()

        self.proxyModel = InventoryProxyModel()
        self.proxyModel.setSourceModel(self.dataModel)

        # set up the inventory table
        self.tableView.setModel(self.proxyModel)
        self.tableView.verticalHeader().show()
        self.tableView.horizontalHeader().setResizeMode(QHeaderView.Stretch)
        self.tableView.horizontalHeader()
        self.tableView.setSortingEnabled(True)
        self.tableView.setSelectionMode(QAbstractItemView.SingleSelection)

        # setup the tool button
        self.toolButton_Load.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolLoadMenu = QMenu(self.toolButton_Load)
        self.toolButton_Load.setMenu(self.toolLoadMenu)

        actionRtLoad = QAction(self.toolLoadMenu)
        actionRtLoad.setText('RogueTech')
        actionRtLoad.triggered.connect(self.load_rt_pressed)
        self.toolLoadMenu.addAction(actionRtLoad)

        actionBtaLoad = QAction(self.toolLoadMenu)
        actionBtaLoad.setText('BattleTech Advanced')
        actionBtaLoad.triggered.connect(self.load_bta_pressed)
        self.toolLoadMenu.addAction(actionBtaLoad)

        actionBtiLoad = QAction(self.toolLoadMenu)
        actionBtiLoad.setText('BattleTech Invasion')
        actionBtiLoad.triggered.connect(self.load_bti_pressed)
        self.toolLoadMenu.addAction(actionBtiLoad)

        # set up the the styling
        # ToDo: make this user adjustable in the future
        self.styleSkins = ['No Skin', 'Dark Style', 'Dark Orange', 'QT Dark', 'QDarkStyle']

        self.currentSkin = 'QDarkStyle'
        self.set_skin('QDarkStyle') # set the default skin

        self.debugFlag = False

        # Signal setup
        self.pushButton_load.pressed.connect(self.load_pressed)
        # self.pushButton_LoadDisk.pressed.connect(self.load_rt_pressed)
        # self.pushButton_LoadDiskBta.pressed.connect(self.load_bta_pressed)
        # self.pushButton_LoadDiskBti.pressed.connect(self.load_bti_pressed)
        self.pushButton_loadTags.pressed.connect(self.load_tags_pressed)
        self.pushButton_editHardpoints.pressed.connect(self.EditHardPoints)
        self.checkBox_exportRtWikiData.stateChanged.connect(self.updateRtWikiExport)
        self.checkBox_exportPrefabs.stateChanged.connect(self.updateWeaponPrefabExport)
        self.checkBox_removeUnitCustom.stateChanged.connect(self.updateRemoveUnitCustom)
        self.checkBox_addUnitRelease.stateChanged.connect(self.updateAddUnitRelease)
        self.checkBox_exportNewWeaponId.stateChanged.connect(self.updateUseWeaponId)
        self.checkBox_reorderDef.stateChanged.connect(self.updateReorderDef)

        self.tableView.setContextMenuPolicy(Qt.ActionsContextMenu)

        self.actionWrite = QAction(self.tableView)
        self.actionWrite.setText('WriteDef')
        self.actionWrite.triggered.connect(self._writedef)
        self.tableView.addAction(self.actionWrite)
        self.actionWrite.setVisible(False)

        # add actions to the right-click menu
        actionRename = QAction(self.tableView)
        actionRename.setText('Set Mech Name/Model')
        actionRename.triggered.connect(self.setMechNameAndModel)
        self.tableView.addAction(actionRename)

        actionDescription = QAction(self.tableView)
        actionDescription.setText('Edit Description')
        actionDescription.triggered.connect(self._editDescription)
        self.tableView.addAction(actionDescription)

        actionYang = QAction(self.tableView)
        actionYang.setText('Edit Yangs Thoughts')
        actionYang.triggered.connect(self._editYangsThoughts)
        self.tableView.addAction(actionYang)

        actionATag = QAction(self.tableView)
        actionATag.setText('Apply Tag set file')
        actionATag.triggered.connect(self._applyTagSet)
        self.tableView.addAction(actionATag)

        actionMTag = QAction(self.tableView)
        actionMTag.setText('View/Edit Mech Tags')
        actionMTag.triggered.connect(self._editMechTags)
        self.tableView.addAction(actionMTag)

        actionCTag = QAction(self.tableView)
        actionCTag.setText('View/Edit Chassis Tags')
        actionCTag.triggered.connect(self._editChassisTags)
        self.tableView.addAction(actionCTag)

        actionRole = QAction(self.tableView)
        actionRole.setText('Set StockRole')
        actionRole.triggered.connect(self.setStockRole)
        self.tableView.addAction(actionRole)

        actionArmor = QAction(self.tableView)
        actionArmor.setText('Set Max Armor/Structure')
        actionArmor.triggered.connect(self.setArmorAndStruct)
        self.tableView.addAction(actionArmor)

        actionAActuator = QAction(self.tableView)
        actionAActuator.setText('Edit Arm Actuator Limits')
        actionAActuator.triggered.connect(self.setArmActuatorLimits)
        self.tableView.addAction(actionAActuator)

        actionAvariant = QAction(self.tableView)
        actionAvariant.setText('Edit Assembly Variant')
        actionAvariant.triggered.connect(self.setAssemblyVariant)
        self.tableView.addAction(actionAvariant)

        actionChassisDefaults = QAction(self.tableView)
        actionChassisDefaults.setText('Edit Chassis Defaults')
        actionChassisDefaults.triggered.connect(self.setChassisDefaults)
        self.tableView.addAction(actionChassisDefaults)

        actionImportCustom = QAction(self.tableView)
        actionImportCustom.setText('Import Custom Chassis Data')
        actionImportCustom.triggered.connect(self._importCustomBlock)
        self.tableView.addAction(actionImportCustom)

        actionExport = QAction(self.tableView)
        actionExport.setText('Export Selected Item')
        actionExport.triggered.connect(self._exportItem)
        self.tableView.addAction(actionExport)

        actionDelete = QAction(self.tableView)
        actionDelete.setText('Delete Mech')
        actionDelete.triggered.connect(self.deleteMech)
        self.tableView.addAction(actionDelete)

        QShortcut(QKeySequence(Qt.CTRL + Qt.ALT + Qt.Key_D), self, self.setDebugFlag)

    def setDebugFlag(self):
        """
        activate/deactivate debug mode

        :return:
        :rtype:
        """
        self.debugFlag = not self.debugFlag
        self.actionWrite.setVisible(self.debugFlag)

    def _writedef(self):
        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            str_write_dir = QFileDialog.getSaveFileName(self, caption="Data File", dir=self.Settings.LastExportPath)[0]

            if str_write_dir != '':
                self.dataModel.WriteMech(lst_idx[0], str_write_dir)
                QMessageBox.information(self, 'Operation Success', 'Wrote Mech to disk')

    def _exportItem(self):
        try:
            lst_idx = []
            for item in sorted(self.tableView.selectedIndexes()):
                lst_idx.append(self.proxyModel.mapToSource(item))
            if len(lst_idx) > 0:
                str_write_dir = QFileDialog.getExistingDirectory(self, caption="Data Export Directory", dir=self.Settings.LastExportPath)

                if str_write_dir != '':
                    self.Settings.LastExportPath = str_write_dir
                    self.Settings.toFile(self.Settings.Path)
                    if self.Settings.RemoveUnitCustom or self.Settings.AddUnitRelease:
                        tags = self.dataModel.getTagSet(lst_idx[0], False)
                        if self.Settings.RemoveUnitCustom:
                            for tag in tags:
                                if tag == 'unit_custom':
                                    tags.remove(tag)
                                    break
                        if self.Settings.AddUnitRelease:
                            for tag in tags:
                                if tag == 'unit_release':
                                    break
                            else:
                                tags.append('unit_release')
                        self.dataModel.setTagSet(lst_idx[0], tags, False)
                    if len(self.MechValidationTags) > 0:
                        tags = self.dataModel.getTagSet(lst_idx[0], False)
                        invalTags = []
                        for tag in tags:
                            if tag not in self.MechValidationTags:
                                invalTags.append(tag)
                        if len(invalTags) > 0:
                            QMessageBox.warning(self, 'Invalid Mech Tags', 'Some mech tags are not valid:\n{0}'.format('\n'.join(invalTags)))
                            return
                    if len(self.ChassisValidationTags) > 0:
                        tags = self.dataModel.getTagSet(lst_idx[0], True)
                        invalTags = []
                        for tag in tags:
                            if tag not in self.ChassisValidationTags:
                                invalTags.append(tag)
                        if len(invalTags) > 0:
                            QMessageBox.warning(self, 'Invalid Chassis Tags', 'Some chassis tags are not valid:\n{0}'.format('\n'.join(invalTags)))
                            return
                    tags = self.dataModel.getTagSet(lst_idx[0], False)
                    for tag in tags:
                        if tag == 'unit_mechtool':
                            break
                    else:
                        tags.append('unit_mechtool')
                    self.dataModel.setTagSet(lst_idx[0], tags, False)
                    tags = self.dataModel.getTagSet(lst_idx[0], True)
                    for tag in tags:
                        if tag == 'unit_mechtool':
                            break
                    else:
                        tags.append('unit_mechtool')
                    self.dataModel.setTagSet(lst_idx[0], tags, True)
                    if not str_write_dir.endswith(os.sep):
                        str_write_dir += os.sep
                    multi = CostCalculator.StandardMulitplier

                    keys = sorted(self.MultiplierCost.keys(), key=str.lower)
                    idex = keys.index(self.StandardMech)
                    output = QInputDialog.getItem(self, 'Select Mech Type', 'Mech Type:\t\t\t\t', keys, current=idex, editable=False)
                    if output[1]:
                        ret = self.dataModel.ExportRecords(lst_idx[0], str_write_dir, self.MultiplierCost[output[0]],
                                                           self.checkBox_exportRtWikiData.isChecked(), self.checkBox_exportPrefabs.isChecked(),
                                                           self.checkBox_exportNewWeaponId.isChecked(), self.checkBox_reorderDef.isChecked())
                        if ret:
                            QMessageBox.information(self, 'Operation Success', 'Exported Mech & Chassis Defs to disk')
                        else:
                            QMessageBox.critical(self, 'Operation Failed', ' Failed to exported Mech & Chassis Defs to disk')
        except:
            trace = traceback.format_exc()
            QMessageBox.critical(self, "Void Kitty Strikes!", "Void Kitty has decreed that this export shall not work, \n it would be great if you could report this to Jamie Wolf#5379 on discord so that he can tame her again:\n\n{0}".format(trace))

    def _editMechTags(self):
        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            obj_dialog = GenericArrayEditor(self.dataModel.getTagSet(lst_idx[0], False), self.MechValidationTags, 'Add Tag',
                                            'Remove Selected Tags')
            obj_dialog.setWindowTitle('Edit Mech Tags')
            obj_dialog.setWindowIcon(self.windowIcon())
            obj_dialog.exec_()
            self.dataModel.setTagSet(lst_idx[0], obj_dialog.Tags, False)

    def _editChassisTags(self):
        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            obj_dialog = GenericArrayEditor(self.dataModel.getTagSet(lst_idx[0], True), self.ChassisValidationTags, 'Add Tag',
                                            'Remove Selected Tags')
            obj_dialog.setWindowTitle('Edit Chassis Tags')
            obj_dialog.setWindowIcon(self.windowIcon())
            obj_dialog.exec_()
            self.dataModel.setTagSet(lst_idx[0], obj_dialog.Tags, True)

    def _editDescription(self):
        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            obj_dialog = DescriptionEditor(self.dataModel.getDescription(lst_idx[0]), "Edit Description", parent=self)
            obj_dialog.setWindowIcon(self.windowIcon())
            result = obj_dialog.exec_()
            if result == QDialog.Accepted:
                self.dataModel.setDescription(lst_idx[0], obj_dialog.Text)

    def _editYangsThoughts(self):
        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            obj_dialog = DescriptionEditor(self.dataModel.getYangsThoughts(lst_idx[0]), "Edit Yang's Thoughts", parent=self)
            obj_dialog.setWindowIcon(self.windowIcon())
            result = obj_dialog.exec_()
            if result == QDialog.Accepted:
                self.dataModel.setYangsThoughts(lst_idx[0], obj_dialog.Text)

    def _applyTagSet(self):

        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            str_write_dir = QFileDialog.getOpenFileName(self, caption="Load Tag set", dir=self.Settings.LastTagApplyPath,
                                        filter="Tag data (*.json)")[0]

            if str_write_dir != '':
                self.Settings.LastTagApplyPath = str_write_dir
                self.Settings.toFile(self.Settings.Path)
                data = readJsonFile(str_write_dir)
                bol_data = False
                if data is not None:
                    if 'MechTags' in data:
                        currentTags = self.dataModel.getTagSet(lst_idx[0], False)
                        newTags = data['MechTags']
                        for tag in newTags:
                            if tag not in currentTags:
                                currentTags.append(tag)
                        self.dataModel.setTagSet(lst_idx[0], currentTags, False)
                        bol_data = True
                    if 'ChassisTags' in data:
                        currentTags = self.dataModel.getTagSet(lst_idx[0], True)
                        newTags = data['ChassisTags']
                        for tag in newTags:
                            if tag not in currentTags:
                                currentTags.append(tag)
                        self.dataModel.setTagSet(lst_idx[0], currentTags, True)
                        bol_data = True
                if bol_data:
                    QMessageBox.information(self, 'Operation Success', 'Tag set applied')
                else:
                    QMessageBox.critical(self, 'Operation Failed', 'Failed to load any tags')

    def _importCustomBlock(self):

        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            str_write_dir = QFileDialog.getOpenFileName(self, caption="Import Custom Block from Chassis", dir=self.Settings.LastRtInstallPath,
                                        filter="ChassisDef(*.json)")[0]

            if str_write_dir != '':
                data = readJsonFile(str_write_dir)
                bol_data = False
                if data is not None:
                    if 'Custom' in data:
                        self.dataModel.ImportCustomBlock(lst_idx[0], data['Custom'])
                        bol_data = True
                if bol_data:
                    QMessageBox.information(self, 'Operation Success', 'Custom Block Imported!')
                else:
                    QMessageBox.critical(self, 'Operation Failed', 'Failed to load custom block')


    def closeEvent(self, event):
        """
        handle shutdown gracefully

        :param event:
        :type event:
        :return:
        :rtype:
        """
        event.accept()

    def update_skin(self):
        """
        change the skin of the UI

        :return:
        :rtype:
        """
        str_skin = self.sender().text()
        self.set_skin(str_skin)

    def read_skin(self, str_skin):

        mfile = QFile(":/stylesheets/{0}".format(str_skin))
        mfile.open(QFile.ReadOnly)
        style = mfile.readAll()
        mfile.close()
        return style.data()

    def set_skin(self, str_skin):
        """
        apply a stylesheet operation

        :param str_skin: the skin to change to, eithr an option in self.skinStyles or a builtin one
        :type str_skin: str
        :return:
        :rtype:
        """
        # print 'Selecting Skin: {0}'.format(str_skin)
        self.currentSkin = str_skin
        if str_skin in QStyleFactory.keys():
            self.qapp.setStyle(QStyleFactory.create(str_skin))
        else:

            mfile = QFile(":/stylesheets/{0}".format(str_skin))
            mfile.open(QFile.ReadOnly)
            style = mfile.readAll()
            self.qapp.setStyleSheet(style.data())
            mfile.close()

    def b64encode(self, bt_data, addLocal=False):
        data = standard_b64encode(bt_data)
        if addLocal:
            if self.LocalSettingsData != '':
                data += self.LocalMarker + self.LocalSettingsData
        return data

    def b64decode(self, bt_data, keepLocal=False):
        if self.LocalMarker in bt_data:
            lst_data = bt_data.split(self.LocalMarker)
            data = lst_data[0]
            if keepLocal:
                self.LocalSettingsData = lst_data[1]
            else:
                self.LocalSettingsData = ''
        else:
            data = bt_data
        return standard_b64decode(data)

    def _decode_data(self, bt_data, keep=False):
        """
        decode data from the cached settings

        :param bt_data:
        :type bt_data: bytes
        :return:
        :rtype: list[dict]
        """
        lst_jsons = []
        y = ProtobufMessage()
        data = self.b64decode(bt_data)
        if self.debugFlag:
            mfile = open('regData.bin', 'wb')
            mfile.write(data)
            mfile.close()
        try:
            y.fromBytes(data, 0)
        except:
            # Attempt a reparse, cutting off the error byte, seems to be some extra data appended to this
            int_err = y.ErrorByte
            y = ProtobufMessage()
            remainder = data[int_err:]
            data = data[0:int_err]
            y.fromBytes(data, 0)
        z = y.get_field(28).data
        a = ProtobufMessage()
        a.fromBytes(z, 0)
        b = ProtobufMessage()
        b.fromBytes(a.get_field(2).data, 0)
        f = FlattenedArray()
        f.fromBytes(b.get_field(1).data, 0)
        for element in f.allElements:
            c = ProtobufMessage()
            c.fromBytes(element, 0)
            jdata = json.loads(c.get_field(1).data, object_pairs_hook=OrderedDict, object_hook=OrderedDict)
            lst_jsons.append(jdata)
            try:
                self.MdSum = c.get_field(2).data
            except:
                pass
        return lst_jsons

    def _reinject_data(self, bt_data, idx, b64=True):
        y = ProtobufMessage()
        remainder = ''
        try:
            if b64:
                y.fromBytes(self.b64decode(bt_data, True), 0)
            else:
                y.fromBytes(bt_data, 0)
        except:
            # Attempt a reparse, cutting off the error byte, seems to be some extra data appended to this
            int_err = y.ErrorByte
            y = ProtobufMessage()
            if b64:
                data = self.b64decode(bt_data, True)
            else:
                data = bt_data
            remainder = data[int_err:]
            data = data[0:int_err]
            y.fromBytes(data, 0)
        z = y.get_field(28).data
        a = ProtobufMessage()
        a.fromBytes(z, 0)
        b = ProtobufMessage()
        b.fromBytes(a.get_field(2).data, 0)
        f = FlattenedArray()
        f.fromBytes(b.get_field(1).data, 0)
        f.remove_element(idx)
        b.get_field(1).setData(f.toBytes())
        a.get_field(2).setData(b.toBytes())
        y.get_field(28).setData(a.toBytes())
        if b64:
            return self.b64encode(y.toBytes() + remainder, True)
        else:
            return y.toBytes() + remainder

    def load_pressed(self):
        """
        handle when the load button is pressed

        :return:
        :rtype:
        """
        lst_jsons = []
        try:
            self.CloudFile = ''
            BtReg = ConnectRegistry(None, HKEY_CURRENT_USER)
            BtKey = OpenKey(BtReg, "Software\Harebrained Schemes\BATTLETECH")
            valLength = QueryInfoKey(BtKey)
            for i in range(valLength[1]):
                    val = EnumValue(BtKey, i)
                    if 'CachedSettings_' in val[0]:
                        data = val[1]
                        self.LastLoadedData = data
                        lst_jsons = self._decode_data(data)
                        break
            else:
                QMessageBox.critical(self, 'Operation Failed', 'Cached Settings File not found')
                return
        except Exception as e:
            trace = traceback.format_exc()
            QMessageBox.critical(self, 'Operation Failed', '{0}'.format(trace))
            return
        QMessageBox.information(self, 'Operation Success', 'loaded {0} mechs from registry'.format(len(lst_jsons)))
        self.dataModel.update_table(lst_jsons)
        self.LoadReg = True

    def loadFromCachedSettingsFile(self, cachepath, cloudPath):

        str_write_dir = QFileDialog.getExistingDirectory(self, caption="BattleTech Installation",
                                                         dir=self.Settings.LastRtInstallPath)

        if str_write_dir != '':
            self.Settings.LastRtInstallPath = str_write_dir
            self.Settings.toFile(self.Settings.Path)
            if not str_write_dir.endswith(os.sep):
                str_write_dir += os.sep

            path = str_write_dir + cachepath
            self.CloudFile = str_write_dir + cloudPath

            lst_jsons = []
            try:
                if os.path.exists(path):
                    mfile = open(path, 'rb')
                    data = mfile.read()
                    mfile.close()
                    self.LastLoadedData = data
                    lst_jsons = self._decode_data(data)

                else:
                    QMessageBox.critical(self, 'Operation Failed', 'Cached Settings File not found')
                    return
            except Exception as e:
                trace = traceback.format_exc()
                QMessageBox.critical(self, 'Operation Failed', '{0}'.format(trace))
                return
            QMessageBox.information(self, 'Operation Success', 'loaded {0} mechs from registry'.format(len(lst_jsons)))
            self.dataModel.update_table(lst_jsons)
            self.LoadReg = False
            self.LastLoadFile = path


    def load_rt_pressed(self):
        """
        handle when the load rt cache button pressed

        :return:
        :rtype:
        """
        self.loadFromCachedSettingsFile(self.RtCache, self.RtCloud)

    def load_bta_pressed(self):
        """
        handle when the load bta cache button pressed

        :return:
        :rtype:
        """
        self.loadFromCachedSettingsFile(self.BtaCache, self.BtaCloud)


    def load_bti_pressed(self):
        """
        handle when the load bti cache button pressed

        :return:
        :rtype:
        """
        self.loadFromCachedSettingsFile(self.BtiCache, self.BtiCloud)

    def load_tags_pressed(self):
        """
        handle when the load validation tags button pressed

        :return:
        :rtype:
        """
        str_write_dir = QFileDialog.getOpenFileName(self, caption="Load Validation Tags", dir=self.Settings.LastTagsetPath, filter="Tag data (*.json)")[0]

        if str_write_dir != '':
            self.MechValidationTags = []
            self.ChassisValidationTags = []
            self.Settings.LastTagsetPath = str_write_dir
            self.Settings.toFile(self.Settings.Path)
            data = readJsonFile(str_write_dir)
            bol_data = False
            if data is not None:
                if 'MechTags' in data:
                    self.MechValidationTags = data['MechTags']
                    bol_data = True
                if 'ChassisTags' in data:
                    self.ChassisValidationTags = data['ChassisTags']
                    bol_data = True
            if bol_data:
                QMessageBox.information(self, 'Operation Success', 'Loaded Validation Tags')
            else:
                QMessageBox.critical(self, 'Operation Failed', 'Failed to load any tags')

    def write_pressed(self):
        """
        handle when the load button is pressed

        :return:
        :rtype:
        """
        lst_jsons = []
        try:
            BtReg = ConnectRegistry(None, HKEY_CURRENT_USER)

            BtKey = OpenKey(BtReg, "Software\Harebrained Schemes\BATTLETECH")
            valLength = QueryInfoKey(BtKey)
            for i in range(valLength[1]):
                    val = EnumValue(BtKey, i)
                    if 'CachedSettings_' in val[0]:
                        data = val[1]
                        x = open('regData.bin', 'wb')
                        x.write(self.b64decode(data))
                        x.close()
                        QMessageBox.information(self, 'Operation Success', 'Wrote Reg Data to regData.bin')
                        break

            else:
                raise Exception('Cached Settings Not Found')
        except Exception as e:
            trace = traceback.format_exc()
            QMessageBox.critical(self, 'Operation Failed', '{0}'.format(trace))
            return

    def updateRtWikiExport(self):
        self.Settings.RtWikiExportForTim = self.checkBox_exportRtWikiData.isChecked()
        self.Settings.toFile(self.SettingsFile)

    def updateWeaponPrefabExport(self):
        self.Settings.ExportWeaponPrefabs = self.checkBox_exportPrefabs.isChecked()
        self.Settings.toFile(self.SettingsFile)

    def updateRemoveUnitCustom(self):
        self.Settings.RemoveUnitCustom = self.checkBox_removeUnitCustom.isChecked()
        self.Settings.toFile(self.SettingsFile)

    def updateAddUnitRelease(self):
        self.Settings.AddUnitRelease = self.checkBox_addUnitRelease.isChecked()
        self.Settings.toFile(self.SettingsFile)

    def updateUseWeaponId(self):
        self.Settings.UseWeaponId = self.checkBox_exportNewWeaponId.isChecked()
        self.Settings.toFile(self.SettingsFile)

    def updateReorderDef(self):
        self.Settings.ReOrderDefs = self.checkBox_reorderDef.isChecked()
        self.Settings.toFile(self.SettingsFile)

    def setMechNameAndModel(self):

        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            initVals = self.dataModel.getNameAndModel(lst_idx[0])

            dialog = MechNameSetter()
            dialog.setWindowFlags(dialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
            dialog.setWindowIcon(self.windowIcon())
            dialog.lineEdit_Model.setText(initVals[0])
            dialog.lineEdit_Variant.setText(initVals[1])
            result = dialog.exec_()
            if result == QDialog.Accepted:
                self.dataModel.setNameAndModel(lst_idx[0], dialog.lineEdit_Model.text(), dialog.lineEdit_Variant.text())

    def setStockRole(self):

        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            initVals = self.dataModel.getStockRole(lst_idx[0])
            # Tabs make the dialog a good size
            lst_data = QInputDialog.getText(self, 'Set StockRole', 'Role\t\t\t\t\t\t', text=initVals)
            if lst_data[1]:
                self.dataModel.setStockRole(lst_idx[0], lst_data[0])

    def setArmActuatorLimits(self):

        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            initVals = self.dataModel.getArmActuatorLimits(lst_idx[0])
            if initVals[0] is None:
                initVals[0] = ActuatorLimiter.NotSet
            if initVals[1] is None:
                initVals[1] = ActuatorLimiter.NotSet

            dialog = ActuatorLimiter()
            dialog.setWindowFlags(dialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
            dialog.setWindowIcon(self.windowIcon())
            dialog.comboBox_right.setCurrentIndex(ActuatorLimiter.IdxList.index(initVals[0]))
            dialog.comboBox_left.setCurrentIndex(ActuatorLimiter.IdxList.index(initVals[1]))
            result = dialog.exec_()
            if result == QDialog.Accepted:
                self.dataModel.setArmActuatorLimits(lst_idx[0], dialog.comboBox_right.currentText(), dialog.comboBox_left.currentText())

    def setAssemblyVariant(self):

        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            initVals = self.dataModel.getAssemblyVariant(lst_idx[0])

            dialog = AssemblyVariant()
            dialog.setWindowFlags(dialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
            dialog.setWindowIcon(self.windowIcon())
            dialog.lineEdit_Model.setText(initVals[0])
            dialog.checkBox_Exclude.setChecked(initVals[1])
            dialog.checkBox_Include.setChecked(initVals[2])
            result = dialog.exec_()
            if result == QDialog.Accepted:
                self.dataModel.setAssemblyVariant(lst_idx[0], dialog.lineEdit_Model.text(), dialog.checkBox_Exclude.isChecked(), dialog.checkBox_Include.isChecked())

    def setChassisDefaults(self):

        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            initVals = self.dataModel.getChassisDefaults(lst_idx[0])

            dialog = ChassisDefaultEditor(initVals)
            dialog.setWindowFlags(dialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
            dialog.setWindowIcon(self.windowIcon())
            result = dialog.exec_()
            if dialog.SaveOnExit:
                self.dataModel.setChassisDefaults(lst_idx[0], dialog.Data)

    def setArmorAndStruct(self):

        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            initVals = self.dataModel.getArmorData(lst_idx[0])

            dialog = ArmorStructEditor(initVals)
            dialog.setWindowFlags(dialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
            dialog.setWindowIcon(self.windowIcon())
            result = dialog.exec_()
            if dialog.SaveOnExit:
                self.dataModel.setArmorData(lst_idx[0], dialog.Data)

    def EditHardPoints(self):

        if self.LastLoadedData != '':
            dialog = HardPointEditor(self.dataModel.inventoryRecords)
            dialog.setWindowIcon(self.windowIcon())
            dialog.exec_()
            if dialog.SaveOnExit:
                data = dialog.Data
                # if not self.LoadReg:
                #     mfile = open(self.LastLoadFile, 'wb')
                #     mfile.write(data)
                #     mfile.close()
                # else:
                #     BtReg = ConnectRegistry(None, HKEY_CURRENT_USER)
                #
                #     BtKey = OpenKey(BtReg, "Software\Harebrained Schemes\BATTLETECH", 0, KEY_ALL_ACCESS)
                #     valLength = QueryInfoKey(BtKey)
                #     for i in range(valLength[1]):
                #         val = EnumValue(BtKey, i)
                #         if 'CachedSettings_' in val[0]:
                #             SetValueEx(BtKey, val[0], 0, REG_BINARY, data)
                #             break
                self.dataModel.update_table(data)

    def deleteMech(self):

        lst_idx = []
        for item in sorted(self.tableView.selectedIndexes()):
            lst_idx.append(self.proxyModel.mapToSource(item))
        if len(lst_idx) > 0:
            ret = QMessageBox.question(self, 'Confirm Delete', 'Delete the selected Mech?', QMessageBox.Yes | QMessageBox.No)
            if ret == QMessageBox.Yes:
                if self.CloudFile == '':
                    str_file_to_load = QFileDialog.getOpenFileName(self, caption="Select Cloud Settings for Writeback",
                                                                   filter="Cloud Settings (settings_cloud.sav)")[0]
                    if str_file_to_load != '':
                        self.CloudFile = str_file_to_load
                    else:
                        return
                self.dataModel.deleteRecord(lst_idx[0])
                self.LastLoadedData = self._reinject_data(self.LastLoadedData, lst_idx[0].row())
                if not self.LoadReg:
                    mfile = open(self.LastLoadFile, 'wb')
                    mfile.write(self.LastLoadedData)
                    mfile.close()
                else:
                    BtReg = ConnectRegistry(None, HKEY_CURRENT_USER)

                    BtKey = OpenKey(BtReg, "Software\Harebrained Schemes\BATTLETECH", 0, KEY_ALL_ACCESS)
                    valLength = QueryInfoKey(BtKey)
                    for i in range(valLength[1]):
                        val = EnumValue(BtKey, i)
                        if 'CachedSettings_' in val[0]:
                            SetValueEx(BtKey, val[0], 0, REG_BINARY, self.LastLoadedData)
                            break
                mfile = open(self.CloudFile, 'rb')
                clouddata = mfile.read()
                mfile.close()
                clouddata = self._reinject_data(clouddata, lst_idx[0].row(), b64=False)
                mfile = open(self.CloudFile, 'wb')
                mfile.write(clouddata)
                mfile.close()


if __name__ == '__main__':

    rootLogger.info('----- BT Mechporter v{0} -----'.format(STR_VERSION))

    obj_main_app = QApplication(sys.argv)
    obj_main_window = BattleTechMechPorter(obj_main_app)
    obj_main_window.show()
    obj_main_app.exec_()