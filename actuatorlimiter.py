from PySide.QtCore import *
from PySide.QtGui import *


import sys
import time
import os
import os.path
from actuatorDialog import Ui_Dialog_ActuatorLimits


class ActuatorLimiter(QDialog, Ui_Dialog_ActuatorLimits):

    """
    the reputation tab handler

    """

    NotSet = 'Hand'
    Upper = 'Upper'
    Lower = 'Lower'
    IdxList = [NotSet, Upper, Lower]

    def __init__(self, parent=None):
        """
        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(ActuatorLimiter, self).__init__(parent)
        self.setupUi(self)
        self.comboBox_left.addItems([self.NotSet, self.Upper, self.Lower])
        self.comboBox_right.addItems([self.NotSet, self.Upper, self.Lower])