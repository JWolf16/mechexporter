from PySide.QtCore import *
from PySide.QtGui import *


import sys
import time
import os
import os.path
from arrayeditorui import Ui_Form_arrayEditor
from models import GenericArrayModel, GenericArrayProxyModel

class GenericArrayEditor(QDialog, Ui_Form_arrayEditor):

    """
    the generic array tab handler

    """

    def __init__(self, tags_object, valid_set, str_add_text=None, str_delete_text=None, parent=None):
        """

        :param fileop: a flattened array of tags to use for data
        :type fileop: libbtsave.FlattenedArray
        :param obj_settings: the preference settings
        :type obj_settings: libbtsave.SettingsStore
        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(GenericArrayEditor, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.TagSet = tags_object

        self.pushButton_Done.pressed.connect(self.close)

        self.dataModel = GenericArrayModel()
        self.dataModel.update_table(self.TagSet, valid_set)

        self.proxyModel = GenericArrayProxyModel()
        self.proxyModel.setSourceModel(self.dataModel)

        # set up the inventory table
        self.tableView.setModel(self.proxyModel)
        self.tableView.verticalHeader().show()
        self.tableView.horizontalHeader().setResizeMode(0, QHeaderView.Stretch)
        self.tableView.setSortingEnabled(True)
        self.tableView.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.tableView.setContextMenuPolicy(Qt.ActionsContextMenu)

        self.Logged = False

        if str_delete_text is not None:
            actionRemove = QAction(self.tableView)
            actionRemove.setText(str_delete_text)
            actionRemove.triggered.connect(self._removeItems)
            self.tableView.addAction(actionRemove)

        if str_add_text is not None:
            actionAdd = QAction(self.tableView)
            actionAdd.setText(str_add_text)
            actionAdd.triggered.connect(self._addTag)
            self.tableView.addAction(actionAdd)

    @property
    def Tags(self):
        return self.dataModel.tagSet

    def _removeItems(self):
        lst_keys = []
        for item in sorted(self.tableView.selectedIndexes()):
            obj_idx = self.proxyModel.mapToSource(item)
            lst_keys.append(obj_idx)
        self.dataModel.removeTags(lst_keys)
        self.Logged = True

    def _addTag(self):
        lst_data = QInputDialog.getText(self, 'Add Item', 'Tag to Add')
        if lst_data[1]:
            self.dataModel.addTag(lst_data[0])
            self.Logged = True
