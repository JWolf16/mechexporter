from PySide.QtCore import *
from PySide.QtGui import *


from AssemblyVariantDialog import Ui_Dialog_AssemblyVariant


class AssemblyVariant(QDialog, Ui_Dialog_AssemblyVariant):

    """
    the reputation tab handler

    """

    def __init__(self, parent=None):
        """
        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(AssemblyVariant, self).__init__(parent)
        self.setupUi(self)