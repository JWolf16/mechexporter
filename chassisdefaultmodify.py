from PySide.QtCore import *
from PySide.QtGui import *


from ChassisDefaultsDialog import Ui_Dialog_AddChassisDefault


class ChassisDefaultsModify(QDialog, Ui_Dialog_AddChassisDefault):

    """
    the reputation tab handler

    """

    def __init__(self, parent=None):
        """
        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(ChassisDefaultsModify, self).__init__(parent)
        self.setupUi(self)