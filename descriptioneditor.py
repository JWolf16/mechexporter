from PySide.QtCore import *
from PySide.QtGui import *


import sys
import time
import os
import os.path
from descriptioneditdialog import Ui_DialogDescriptionEdit


class DescriptionEditor(QDialog, Ui_DialogDescriptionEdit):

    """
    the generic array tab handler

    """

    def __init__(self, str_description, str_title, parent=None):
        """

        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(DescriptionEditor, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(str_title)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.textEdit_Description.setAcceptRichText(True)
        self.textEdit_Preview.setStyleSheet(self.parent().read_skin('No Skin'))
        self.textEdit_Preview.setAcceptRichText(True)
        self.textEdit_Preview.setReadOnly(True)

        self.textEdit_Description.setPlainText(str_description)
        self.updateText()

        self.textEdit_Description.textChanged.connect(self.updateText)

    def updateText(self):
        try:
            self.textEdit_Preview.setHtml(self.textEdit_Description.toPlainText().replace('<color=', '<font color=').replace('</color>', '</font>'))
            self.textEdit_Preview.show()
        except:
            pass

    @property
    def Text(self):
        return self.textEdit_Description.toPlainText()
