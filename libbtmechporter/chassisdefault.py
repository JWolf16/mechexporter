from collections import OrderedDict


class ChassisDefault(object):

    def __init__(self):
        self.Category = ''
        self.Id = ''
        self.Location = ''
        self.Type = ''

    def toJson(self):
        dct = OrderedDict()
        dct['CategoryID'] = self.Category
        dct['DefID'] = self.Id
        dct['Location'] = self.Location
        dct['Type'] = self.Type

        return dct

    def fromJson(self, dct_data):
        self.Category = dct_data['CategoryID']
        self.Id = dct_data['DefID']
        self.Location = dct_data['Location']
        self.Type = dct_data['Type']