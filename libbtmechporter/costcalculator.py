

class CostCalculator(object):

    SuperLightBase = 10000
    LightBase = 40000
    MediumBase = 60000
    HeavyBase = 80000
    AssaultBase = 100000

    SuperLightPerTon = 3000
    LightPerTon = 4000
    MediumPerTon = 6000
    HeavyPerTon = 8000
    AssaultPerTon = 10000

    StandardMulitplier = 1.00
    PirateMulitplier = 1.00
    PowerArmorMulitplier = 2.00
    PrimativeMulitplier = 0.95
    ClannerMulitplier = 1.30
    EliteMulitplier = 1.20
    JkHeroMulitplier = 1.10

    def __init__(self, mechdata):
        self.MechData = mechdata
        self.ChassisDefCost = 0
        self.MechDefCost = 0
        self.SimGamePartCost = 0
        self.PartialCost = 0

        self.weight = self.MechData['Chassis']['Tonnage']

        self.BaseCost = self.SuperLightBase
        self.TonnageMultiplier = self.SuperLightPerTon

        if self.weight >= 80:
            self.BaseCost = self.AssaultBase
            self.TonnageMultiplier = self.AssaultPerTon
        elif self.weight >= 60:
            self.BaseCost = self.HeavyBase
            self.TonnageMultiplier = self.HeavyPerTon
        elif self.weight >= 40:
            self.BaseCost = self.MediumBase
            self.TonnageMultiplier = self.MediumPerTon
        elif self.weight >= 20:
            self.BaseCost = self.LightBase
            self.TonnageMultiplier = self.LightPerTon

        self.HardPointCount = 0
        for item in self.MechData['Chassis']['Locations']:
            for hp in item['Hardpoints']:
                if 'Omni' in hp:
                    if hp['Omni']:
                        self.HardPointCount += 4
                    else:
                        self.HardPointCount += 1
                else:
                    self.HardPointCount += 1

        chassisBase = self.BaseCost + (self.TonnageMultiplier * self.weight)
        HpBase = chassisBase + (self.HardPointCount * 100)
        HpMultiplier = 1.00 + (float(self.HardPointCount) * 0.01)
        self.PartialCost = HpBase * HpMultiplier


    def calculateCost(self, mechTypeMultiplier):

        typeCost = int(self.PartialCost * mechTypeMultiplier)
        self.ChassisDefCost = typeCost * 10
        self.MechDefCost = typeCost * 2
        self.SimGamePartCost = self.ChassisDefCost/5




