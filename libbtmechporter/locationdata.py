
class LocationData(object):

    def __init__(self):
        self.Location = ''
        self.MaxStruct = 0
        self.MaxArmor = 0
        self.MaxRear = -1

    @property
    def canEditRear(self):
        return self.MaxRear > 0