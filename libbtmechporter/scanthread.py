from PySide.QtCore import QObject, Signal
import os
import os.path
import json5
import json
import traceback
from collections import OrderedDict



class ScanThread(QObject):

    FileScanComplete = Signal(bool, int, int)
    ScanForMechDefs = Signal(str)

    JsonEncodings = ['utf-8', 'cp1252']

    def __init__(self, logger, parent=None):
        super(ScanThread, self).__init__(parent)
        self.Logger = logger
        self.debugFlag = False

        # connect signals
        self.ScanForMechDefs.connect(self._scanModPackDebug)
        self.Success = 0
        self.Failed = 0


    def _prefabNameFix(self, str_path, bol_mod):
        data = None
        str_err = ''
        lst_errs = []
        self.Logger.debug('Reading Json: {0}'.format(str_path))
        with open(str_path, 'rb') as f:
            for encode in self.JsonEncodings:
                f.seek(0)
                try:
                    try:
                        data = json.load(f, encoding=encode, object_pairs_hook=OrderedDict, object_hook=OrderedDict)
                        break
                    except Exception as e:
                        str_err = str(e)
                        f.seek(0)
                        data = json5.load(f, encoding=encode, object_pairs_hook=OrderedDict, object_hook=OrderedDict)
                        break
                except Exception as e:
                    if not bol_mod:
                        self.Logger.error('Reading: {0} failed, Encoder: {2}, Json5 Error: {1}'.format(str_path, str(e), encode))
                        self.Logger.error('Json Error: {0}'.format(str_err))
                        return False
                    else:
                        lst_trace = traceback.format_exc().split('\n')
                        try:
                            lst_errs.append('Reading: {0} failed, Encoder: {2}, Json5 Error: {1}'.format(str_path, str(e), encode))
                            lst_errs.append('Json Error: {0}'.format(str_err))
                        except:
                            self.Logger.error('Could not log failure reason...probably a f***ing unicode problem..print trace instead')
                            for line in lst_trace:
                                self.Logger.error(line)

        if data is None:
            for item in lst_errs:
                self.Logger.warning(item)
        else:
            if 'inventory' in data:
                for item in data['inventory']:
                    if 'prefabName' in item:
                        item['prefabName'] = None
                    if 'hasPrefabName' in item:
                        item['hasPrefabName'] = False
            with open(str_path, 'wb') as f:
                json.dump(data, f, indent=4)
            return True

        return False

    def _scanModPackDebug(self, str_mod_path):
        """
        scan a mod pack directory, debugging only

        :param str_mod_path:
        :type str_mod_path:
        :return:
        :rtype:
        """
        self.Logger.info('Starting Debug Scan of: {0}'.format(str_mod_path))
        self.Success = 0
        self.Failed = 0
        try:
            bol_result = self._scanModPack(str_mod_path)
            self.FileScanComplete.emit(bol_result, self.Success, self.Failed)
        except:
            lst_trace = traceback.format_exc().split('\n')
            self.Logger.critical('Scan fatal error...')
            for line in lst_trace:
                self.Logger.critical(line)
            self.FileScanComplete.emit(False, 0, 0)

    def _scanMod(self, str_mod_path):
        dct_data = {
            'MechDef': 'MECHPART',
        }
        bol_result = True
        if os.path.exists(str_mod_path + 'mod.json'):
            with open(str_mod_path + 'mod.json') as f:
                try:
                    data = json.load(f)
                except:
                    try:
                        f.seek(0)
                        data = json5.load(f)
                    except:
                        self.Logger.error('Failed to read: {0}'.format(str_mod_path + 'mod.json'))
                        raise
                bol_loaded = True
                try:
                    bol_loaded = data['Enabled']
                except KeyError:
                    bol_loaded = True
                if bol_loaded:
                    try:
                        lst_manifest = data['Manifest']
                        for entry in lst_manifest:
                            if entry['Type'] in dct_data:
                                str_scan_path = entry['Path']
                                str_key = dct_data[entry['Type']]
                                if str_scan_path.endswith(os.sep):
                                    str_scan_path = str_scan_path.rstrip(os.sep)
                                if not self._scanDir(str_mod_path + str_scan_path, str_key, [], bol_mod=True):
                                    bol_result = False

                    except KeyError:
                        pass
        return bol_result

    def _scanModPack(self, str_path):
        lst_dir = os.listdir(str_path)
        data_paths = ''
        bol_result = True
        for directory in lst_dir:
            str_mod_path = str_path + os.sep + directory
            if os.path.isdir(str_mod_path):
                # possible mod
                str_mod_path += os.sep
                if not self._scanMod(str_mod_path):
                    bol_result = False
        return bol_result

    def _scanDir(self, str_path, str_key, lst_exclude, bol_mod=False):
        """
        Scan a directory for json assets

        :param str_path: the directory path
        :type str_path: str
        :param str_key: the category of the items (ie. mech, weapon, etc...)
        :type str_key: str
        :param lst_exclude: a list of file names to exclude from the scan
        :type lst_exclude: list[str]
        :param bol_mod: whether or not to error out on malformed or missing json tags
        :type bol_mod: bool
        :return: True on successful scan, False
        :rtype: bool
        """
        if not os.path.exists(str_path):
            self.Logger.warning('Path: {0} does not exist...but is referenced by something, maybe this should be reported to someone'.format(str_path))
            return True
        lst_files = os.listdir(str_path)
        for item in lst_files:
            if os.path.isdir(str_path + os.sep + item):
                if not self._scanDir(str_path + os.sep + item, str_key, lst_exclude):
                    return False
                continue
            # go over every file and if it is a JSON file and is not blacklisted then load it and extract its ID
            for ext in lst_exclude:
                if ext in item.lower():
                    break
            else:
                if item.lower().endswith('.json'):
                    data = self._prefabNameFix(str_path + os.sep + item, bol_mod)
                    if data:
                        self.Success += 1
                    else:
                        self.Failed += 1

        return True
