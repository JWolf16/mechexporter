from libbtsave import BaseXml


class MechPorterSettingsStore(BaseXml):

    XmlType = 'MechPorterSettingsStore'

    def __init__(self):
        self._path = ''
        self.LastRtInstallPath = ''
        self.LastExportPath = ''
        self.LastTagsetPath = ''
        self.LastTagApplyPath = ''
        self.RtWikiExportForTim = False
        self.ExportWeaponPrefabs = False
        self.RemoveUnitCustom = True
        self.AddUnitRelease = True
        self.UseWeaponId = False
        self.ReOrderDefs = True

    def setFilePath(self, str_path):
        self._path = str_path

    @property
    def Path(self):
        return self._path