import openpyxl
import os
import os.path


class WikiExporter(object):


    def __init__(self, str_file):
        self.FilePath = str_file

        bol_write_header = False
        self.Workbook = None
        if os.path.isfile(self.FilePath):
            self.Workbook = openpyxl.load_workbook(self.FilePath)
        else:
            self.Workbook = openpyxl.Workbook()
            bol_write_header = True


        self.WorkSheet = self.Workbook.active
        if self.WorkSheet is None:
            self.WorkSheet = self.Workbook.create_sheet("Mechs")

        if bol_write_header:
            self.WorkSheet.append(['Mech Name', 'Variant', 'Structure', 'Current Armor', 'Max Armor', 'Tonnage',
                                   'Class', 'Meleee Damage', 'DFA Damage', 'Description', 'Ballistic', 'Energy', 'Missle', 'AntiPersonnel', 'Omni', 'JumpJets'])



    def export(self, name, variant, structure, currentArmor, MaxArmor, tonnage, weightClass, meleeDamage, DfaDamage, Description, b, e, m, a, o, jj):
        self.WorkSheet.append([name, variant, structure, currentArmor, MaxArmor, tonnage, weightClass, meleeDamage, DfaDamage, Description, b, e, m, a, o, jj])
        self.Workbook.save(self.FilePath)