from .encoders import ByteEncoder, ByteArrayEncoder, IntEncoder
from .utils import read_raw_file, convert_to_string, convert_to_bytes, read_file
from .metadata import MetaData
from .gamedata import GameData
from hashlib import md5
from math import ceil

class BattleTechSave(object):

    # File signature, if a file doesnt start with this, it isn't a save
    FileHeader = '\x62\x73\x74\x62'

    def __init__(self, str_path=None):
        self._path = str_path
        self.Version = IntEncoder()
        self.TypeHash = ByteArrayEncoder()
        self.MetaDataHash = ByteArrayEncoder()
        self.SaveDataHash = ByteArrayEncoder()
        self.MetaData = MetaData()
        self.DataDivide = IntEncoder()
        self.GameData = GameData()

    def load(self, str_path=None):
        """
        load a save file into memory

        :param str_path: the path of the save file
        :type str_path: str
        :return:
        :rtype:
        """
        if str_path is not None:
            self._path = str_path
        lst_data = read_file(self._path)
        # print 'Data loaded!'
        if lst_data[:4] != self.FileHeader:
            raise ValueError('Incorrect File format header!')
        int_offset = 4
        int_offset = self.Version.fromBytes(lst_data, int_offset)
        int_offset = self.TypeHash.fromBytes(lst_data, int_offset)
        int_offset = self.MetaDataHash.fromBytes(lst_data, int_offset)
        int_offset = self.SaveDataHash.fromBytes(lst_data, int_offset)
        obj_meta = ByteArrayEncoder()
        int_offset = obj_meta.fromBytes(lst_data, int_offset)
        self.MetaData.setData(obj_meta.value)
        int_offset = self.DataDivide.fromBytes(lst_data, int_offset)
        obj_game = ByteArrayEncoder()
        int_offset = obj_game.fromBytes(lst_data, int_offset)
        self.GameData.setData(obj_game.value)
        # print 'Done File Decoding!'
        # print 'Data Indexed: {0}, file length: {1}'.format(int_offset, len(lst_data))


    def save(self, str_file):
        """
        re-serialize the save data and write it to disk

        :param str_file: the file to write to
        :type str_file: str
        :return:
        :rtype:
        """
        m_file = open(str_file, 'wb')

        # for now just write the current metadata hash since we are not changing the metadata, if we do in the future
        # then we will need to recalculate it
        lst_data_start = self.FileHeader + self.Version.toBytes() + self.TypeHash.toBytes() + self.MetaDataHash.toBytes()
        m_file.write(lst_data_start)
        str_save_data = self.GameData.repackedData
        self.SaveDataHash.setValue(self.GameData.Md5)
        m_file.write(self.SaveDataHash.toBytes())
        m_file.write(self.MetaData.repackedData)
        m_file.write(self.DataDivide.toBytes())
        m_file.write(str_save_data)
        m_file.close()
        # print 'MetaH: {0}'.format(self.MetaData.Md5Hex)
        # print 'MetaA: {0}'.format(self.MetaDataHash.hexStr)

    @property
    def path(self):
        return self._path

    def report(self):
        """
        get a string report of this save

        :return:
        :rtype: str
        """

        lst_metahex = []
        ilen = len(self.MetaData.hexStr)
        for x in range(int(ceil(ilen/128.0))):
            lst_metahex.append(self.MetaData.hexStr[x*128:(x*128)+128])
        str_metahex = '\n'.join(lst_metahex)

        # lst_gamehex = []
        # ilen = len(self.GameData.hexStr)
        # for x in range(int(ceil(ilen / 128.0))):
        #     lst_gamehex.append(self.GameData.hexStr[x * 128:(x * 128) + 128])
        # str_gamehex = '\n'.join(lst_gamehex)

        str_ret = "======== BattleTech Save Data ========\n"
        str_ret += 'Path          : {0}\n'.format(self._path)
        str_ret += 'File Version  : {0}\n'.format(self.Version.value)
        str_ret += 'Type Hash     : {0}\n'.format(self.TypeHash.hexStr)
        str_ret += 'Meta Data Hash: {0}\n'.format(self.MetaDataHash.hexStr)
        str_ret += 'Game Data Hash: {0}\n'.format(self.SaveDataHash.hexStr)
        str_ret += 'Data Divide   : {0}\n'.format(self.DataDivide.value)
        str_ret += 'Meta Data size: {0}\n'.format(self.MetaData.size)
        str_ret += 'Meta Data hex : \n{0}\n'.format(str_metahex)
        # str_ret += 'Meta Data     : \n{0}\n'.format(self.MetaData.value)
        str_ret += 'Game Data size: {0}\n'.format(self.GameData.size)
        str_ret += 'Game Data encs: \n{0}\n'.format(self.GameData.encoderSize)
        # str_ret += 'Game Data hex : \n{0}\n'.format(str_gamehex)
        # str_ret += 'Game Data     : \n{0}\n'.format(self.GameData.value)


        return str_ret