from ...data import MechData, LocationLoadoutDef, FlattenedArray, NestedClass, DataCollection, DescriptionDef, EnumMember, MechComponentRef
from ...protobuf import ProtobufMessage, ProtobufField, EWireType
from ...utils import readJsonFile
from ...gamedata import GameData
from .mechbuilderrors import MechBuildErrors
import time
import struct

UNITYPACK_LOADED = False
try:
    import unitypack
    UNITYPACK_LOADED = True
except ImportError:
    pass


class MechDefBuilder(object):

    RefKey = '{0}, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null'

    def __init__(self, obj_gamedata):
        """

        :param obj_gamedata:
        :type obj_gamedata: GameData
        """
        self._mechData = MechData()
        self._gameData = obj_gamedata
        self.ErrCode = MechBuildErrors.Err_Unknown

        self.assets = {
            'Texture2D' : [],
            'Mesh' : []
        }

    def _read_asset_bundle(self, str_file):
        try:
            with open(str_file, "rb") as f:
                bundle = unitypack.load(f)
                for asset in bundle.assets:
                    for id, object in asset.objects.items():
                        # if object.type in ["TextAsset", 'Texture2D', 'Mesh', 'GameObject']:
                        #     data = object.read()
                        #     try:
                        #         print '{0} : {1}'.format(data.name, object.type)
                        #     except:
                        #         pass
                        if object.type in self.assets.keys():
                            data = object.read()
                            self.assets[object.type].append(data.name)

        except Exception as e:
            print e
            print 'Reading: {0} failed, Error: {1}'.format(str_file, str(e))
            raise

    def pick_a_mesh(self):
        for texture in self.assets['Texture2D']:
            if '-msk' in texture:
                return texture

    def pick_a_prefab(self, str_weaponid, lst_prefabs):
        pass

    def loadMechJson(self, str_file, str_chassis_file, json_mechdata=None, json_chassisdata=None):
        self.ErrCode = MechBuildErrors.Err_Unknown
        mechMessage = ProtobufMessage()

        jsonData = None
        if json_mechdata is not None:
            jsonData = json_mechdata
        else:
            jsonData = readJsonFile(str_file)

        chassisData = None
        if json_chassisdata is not None:
            chassisData = json_chassisdata
        else:
            chassisData = readJsonFile(str_chassis_file)

        # self._read_asset_bundle(asset_file)
        if jsonData is None:
            self.ErrCode = MechBuildErrors.Err_InvalidMechDef
            return False
        # print jsonData

        if chassisData is None:
            self.ErrCode = MechBuildErrors.Err_InvalidChassisDef
            return False

        refdeictidx = self._gameData.refContainer.findRefDict('ActiveMechs')[0]
        mechbay = self._gameData.refContainer.getRefDict(refdeictidx)

        baySize = 0
        if self._gameData.dataCollection.findRecord('MechBayPods_Display') is not None:
            baySize = self._gameData.dataCollection.findRecord('MechBayPods_Display').currentValue.value

        # Some saves dont seem to have the display record... not sure why, but if thats the case try this instead
        elif self._gameData.dataCollection.findRecord('MechBayPods') is not None:
            baySize = self._gameData.dataCollection.findRecord('MechBayPods').currentValue.value * 6

        if baySize == 0:
            self.ErrCode = MechBuildErrors.Err_MissingRecord
            return False

        mechbaySlot = None
        for x in range(baySize):
            try:
                someval = mechbay.items[x]
            except KeyError:
                mechbaySlot = x
                break
        else:
            self.ErrCode = MechBuildErrors.Err_NoSpace
            return False

        locations = []
        for item in jsonData['Locations']:
            loadout = LocationLoadoutDef.newItem(item['Location'], item['CurrentArmor'], item['CurrentRearArmor'], item['CurrentInternalStructure'],
                                                 item['AssignedArmor'], item['AssignedRearArmor'])
            locations.append(loadout)
        loadarray = FlattenedArray()
        loadarray.set_ids(9, 757404745)
        for item in locations:
            loadarray.add_element(item.toBytes())

        stats = DataCollection()
        statsNest = NestedClass(stats)
        statsNest.set_ids(9, 3522564105)

        descriptor = DescriptionDef.fromJson(jsonData['Description'])
        descriptorNest = NestedClass(descriptor, bol_use_inner=False)
        descriptorNest.set_ids(10, 4054355552)

        role = EnumMember()
        role.setType(1150142801)
        role.setValue('Undefined')

        tags = FlattenedArray()
        tags.set_ids(7)
        for item in jsonData['MechTags']['items']:
            tags.add_element(str(item))

        # paintjob = self.pick_a_mesh()
        # if paintjob is None:
        #     return False

        mechId = self._gameData.generate_refId()

        mechMessage.add_field(EWireType.LengthDelimited, 1, lst_data=loadarray.toBytes())
        mechMessage.add_field(EWireType.LengthDelimited, 3, lst_data=statsNest.toBytes())
        mechMessage.add_field(EWireType.LengthDelimited, 4, lst_data=str(jsonData['ChassisID']))
        if 'BattleValue' in chassisData:
            mechMessage.add_field(EWireType.Variant, 5, int_value=chassisData['BattleValue'])
        mechMessage.add_field(EWireType.LengthDelimited, 8, lst_data=descriptorNest.toBytes())
        mechMessage.add_field(EWireType.LengthDelimited, 9, lst_data=mechId)
        mechMessage.add_field(EWireType.LengthDelimited, 13, lst_data=role.toBytes())
        # mechMessage.add_field(EWireType.LengthDelimited, 15, lst_data=str(paintjob))
        mechMessage.add_field(EWireType.LengthDelimited, 19, lst_data=tags.toBytes())

        meleeref = MechComponentRef.newItem('Weapon_MeleeAttack', 'CenterTorso', 'Weapon', self._gameData.generate_refId(),
                                            prefabName='chrPrfWeap_generic_melee', hardpointSlot=-1)

        dfaref = MechComponentRef.newItem('Weapon_DFAAttack', 'CenterTorso', 'Weapon',
                                            self._gameData.generate_refId(),
                                            prefabName='chrPrfWeap_generic_melee', hardpointSlot=-1)

        ailaserref = MechComponentRef.newItem('Weapon_Laser_AI_Imaginary', 'CenterTorso', 'Weapon',
                                            self._gameData.generate_refId(),
                                            prefabName='chrPrfWeap_generic_melee', hardpointSlot=-1)

        lst_equipment = []
        lst_equiprefs = []

        for item in jsonData['inventory']:
            prefab = None
            if item['ComponentDefType'] == 'Weapon':
                pass
            itemref = MechComponentRef.newItem(item['ComponentDefID'], item['MountedLocation'], item['ComponentDefType'],
                                               self._gameData.generate_refId(),prefabName=prefab, hardpointSlot=item['HardpointSlot'])
            lst_equipment.append(itemref)
            lst_equiprefs.append(itemref.guid)

        intmech = self._gameData.refContainer.addContainer(self.RefKey.format('BattleTech.MechDef'), mechMessage.toBytes())

        self._gameData.refContainer.addContainer(self.RefKey.format('BattleTech.MechComponentRef'), meleeref.toBytes())
        self._gameData.refContainer.addContainer(self.RefKey.format('BattleTech.MechComponentRef'), dfaref.toBytes())
        self._gameData.refContainer.addContainer(self.RefKey.format('BattleTech.MechComponentRef'), ailaserref.toBytes())

        self._gameData.refContainer.addKeyVal('{0}.MeleeWeaponRef'.format(mechId), meleeref.guid)
        self._gameData.refContainer.addKeyVal('{0}.DFAWeaponRef'.format(mechId), dfaref.guid)
        self._gameData.refContainer.addKeyVal('{0}.ImaginaryLaserRef'.format(mechId), ailaserref.guid)

        for ref in lst_equipment:
            self._gameData.refContainer.addContainer(self.RefKey.format('BattleTech.MechComponentRef'),
                                                     ref.toBytes())

        self._gameData.refContainer.addRefList('{0}.Inventory'.format(mechId), lst_equiprefs)

        mechbay.items[mechbaySlot] = mechId

        self._gameData.reload_mechs()

        self.ErrCode = MechBuildErrors.Err_Success
        return True