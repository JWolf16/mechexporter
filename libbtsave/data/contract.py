from ..protobuf import ProtobufMessage
from .flattenedarray import FlattenedArray
from .nestedclass import NestedClass
from .enummember import EnumMember
from .contractoverride import ContractOverride

class Contract(object):

    def __init__(self):
        self._rawData = None
        self.mainMessage = ProtobufMessage()
        self.guid = ''
        self.state = EnumMember()
        self.contractType = EnumMember()
        self.simulatedResult = EnumMember()
        self.contractOverride = ContractOverride()
        self.overrideNest = NestedClass(self.contractOverride, False)
        self.targetSystem = ''
        self.overrideId = ''

    def fromBytes(self, lst_bytes, int_offset=0):
        try:
            self._rawData = lst_bytes

            self.mainMessage.fromBytes(lst_bytes, 0)
            self.state.fromBytes(self.mainMessage.get_field(9).data)
            self.contractType.fromBytes(self.mainMessage.get_field(14).data)
            self.simulatedResult.fromBytes(self.mainMessage.get_field(20).data)
            self.targetSystem = self.mainMessage.get_field(49).data
            self.guid = self.mainMessage.get_field(60).data
            if self.mainMessage.get_field(61) is not None:
                self.overrideNest.fromBytes(self.mainMessage.get_field(61).data, 0)
            else:
                self.overrideId = self.mainMessage.get_field(55).data

        except Exception as e:
            print str(e)
            raise
        # print 'Decode Time: {0:0.2f}'.format(fl_end - fl_start)
        # print self.pilotMessage.get_field(10).dataStr


    def toBytes(self):

        return self.mainMessage.toBytes()