from ..protobuf import ProtobufMessage
from .flattenedarray import FlattenedArray
from .nestedclass import NestedClass
from .enummember import EnumMember

class ContractOverride(object):

    def __init__(self):
        self._rawData = None
        self.mainMessage = ProtobufMessage()
        self.name = ''
        self.state = EnumMember()
        self.contractType = EnumMember()
        self.simulatedResult = EnumMember()
        self.targetSystem = ''

    def fromBytes(self, lst_bytes, int_offset):
        try:
            self._rawData = lst_bytes

            self.mainMessage.fromBytes(lst_bytes, 0)
            self.name = self.mainMessage.get_field(1).data

        except Exception as e:
            print str(e)
            raise
        # print 'Decode Time: {0:0.2f}'.format(fl_end - fl_start)
        # print self.pilotMessage.get_field(10).dataStr


    def toBytes(self):

        return self.mainMessage.toBytes()