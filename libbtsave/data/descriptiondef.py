from ..utils import convert_to_stringio, convert_to_bytes, convert_to_string
from ..encoders import encoder_factory, ByteArrayEncoder
from cStringIO import StringIO
from hashlib import md5
from .datacollection import DataCollection
from .flattenedarray import FlattenedArray
from .nestedclass import NestedClass
from ..protobuf import ProtobufMessage, EWireType
import time
import struct

class DescriptionDef(object):

    """
    the save data representation of a mech in the company

    """

    def __init__(self):
        """
        init the object

        """
        self._rawData = None
        self._repackedData = None
        self.mainMessage = ProtobufMessage()
        self.name = ''
        self.uiName = ''

    def fromBytes(self, lst_bytes, int_offset=0):
        self._rawData = lst_bytes
        self.mainMessage.fromBytes(lst_bytes, 0)
        self.name = self.mainMessage.get_field(2).data
        if self.mainMessage.get_field(10) is not None:
            self.uiName = self.mainMessage.get_field(10).data
        else:
            self.uiName = self.mainMessage.get_field(1).data.replace('mechdef_', '').replace('_', ' ')

    def toBytes(self):

        return self.mainMessage.toBytes()

    @staticmethod
    def fromJson(obj_json):

        message = ProtobufMessage()
        message.add_field(EWireType.LengthDelimited, 1, lst_data=bytes(obj_json['Id']))
        message.add_field(EWireType.LengthDelimited, 2, lst_data=bytes(obj_json['Name']))
        message.add_field(EWireType.LengthDelimited, 3, lst_data=bytes(obj_json['Details'].encode('utf-8')))
        message.add_field(EWireType.LengthDelimited, 4, lst_data=bytes(obj_json['Icon']))
        message.add_field(EWireType.Variant, 5, int_value=obj_json['Cost'])
        message.add_field(EWireType.Fixed32, 6, lst_data=struct.pack('<f', float(obj_json['Rarity'])))
        message.add_field(EWireType.LengthDelimited, 10, lst_data=bytes(obj_json['UIName']))

        obj = DescriptionDef()
        obj.fromBytes(message.toBytes())
        return obj
