from ..protobuf import ProtobufMessage, Variant, EWireType


class EnumMember(object):

    def __init__(self):
        self._rawData = None
        self.mainMessage = ProtobufMessage()

    def fromBytes(self, lst_bytes):
        # Need to figure out this offset as it changes
        try:
            self._rawData = lst_bytes

            self.mainMessage.fromBytes(lst_bytes, 0)

        except Exception as e:
            print str(e)
            raise


    def toBytes(self):
        return self.mainMessage.toBytes()

    @property
    def type(self):
        return self.mainMessage.get_field(1).dataInt

    @property
    def value(self):
        return self.mainMessage.get_field(2).data

    def setType(self, int_type):
        if self.mainMessage.get_field(1) is not None:
            self.mainMessage.get_field(1).setValue(int_type)
        else:
            self.mainMessage.add_field(EWireType.Variant, 1, int_value=int_type)

    def setValue(self, str_value):
        if self.mainMessage.get_field(2) is not None:
            self.mainMessage.get_field(2).setData(str_value)
        else:
            self.mainMessage.add_field(EWireType.LengthDelimited, 2, lst_data=str_value)