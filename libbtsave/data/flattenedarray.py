from ..protobuf import ProtobufMessage, EWireType, Variant, ProtobufField
from ..utils import convert_to_bytes

class FlattenedArray(object):

    """
    a protobuf message that contains repeated field numbers, used by the save data to contain lists or arrays

    """

    def __init__(self):
        self.id = None
        self.classId = None
        self.elements = []

    def set_ids(self, int_id, int_class=None):
        self.id = ProtobufField()
        self.id.setFieldTag(EWireType.Variant, 1)
        self.id.setValue(int_id)

        if int_class is not None:
            self.classId = ProtobufField()
            self.classId.setFieldTag(EWireType.Variant, 2)
            self.classId.setValue(int_class)


    def fromBytes(self, lst_bytes, int_offset):
        message = ProtobufMessage()
        message.fromBytes(lst_bytes, int_offset)
        self.elements = []
        self.id = message.get_field(1)
        self.classId = message.get_field(2)
        lst_elements = message.get_fields(3)
        for element in lst_elements:
            item = ProtobufMessage()
            item.fromBytes(element.data, 0)
            self.elements.append(item)
        # for element in self.elements:
        #     print element.get_field(1).dataInt
        #     print element.get_field(2).dataStr
        return int_offset

    def toBytes(self):
        message = ProtobufMessage()
        if self.id is not None:
            message.append_field(self.id)
        if self.classId is not None:
            message.append_field(self.classId)
        for element in self.elements:
            message.add_field(EWireType.LengthDelimited, 3, lst_data=element.toBytes())
        var = Variant()
        var.setValue(len(self.elements))
        message.add_field(EWireType.LengthDelimited, 4, lst_data=var.toBytes())
        return message.toBytes()

    def clear_elements(self):
        """
        clear all elements from the array

        :return:
        :rtype:
        """
        self.elements = []

    @property
    def size(self):
        """
        get the sie of the array

        :return: the number of elements in the array
        :rtype: int
        """
        return len(self.elements)

    @property
    def allElements(self):
        """
        get all the elements

        :return:
        :rtype: lisr[str]
        """
        lst_items = []
        for element in self.elements:
                lst_items.append(element.get_field(2).dataStr)
        return lst_items

    def updateElement(self, int_idx, str_data):
        """


        :param int_idx:
        :type int_idx:
        :param str_data:
        :type str_data:
        :return:
        :rtype:
        """
        for element in self.elements:
            if element.get_field(1).dataInt == int_idx:
                element.get_field(2).dataStr = str_data


    def checkElement(self, str_data):
        """
        check if an the given data is an element

        :param str_data: the data to check for
        :type str_data: str
        :return: the element number, if found -1 otherwise
        :rtype: int
        """
        for element in self.elements:
            if element.get_field(2).dataStr == str_data:
                return element.get_field(1).dataInt
        return -1

    def find_element(self, str_data):
        """
        find the first elememt that contain a string, returns -1 if no such string is found

        :param str_data: the string to look for
        :type str_data: str
        :return:
        :rtype: int
        """
        for element in self.elements:
            if str_data in element.get_field(2).dataStr:
                return element.get_field(1).dataInt
        return -1

    def get_element(self, int_element_index):
        """
        get the specified element's data

        :param int_element_index: the index of the element
        :type int_element_index: int
        :return: the data as a string
        :rtype: str
        """
        for element in self.elements:
            if element.get_field(1).dataInt == int_element_index:
                return element.get_field(2).dataStr
        return None

    def remove_elements(self, lst_indexes):
        """
        remove a list of items

        :param lst_indexes: a list of indexes to remove
        :type lst_indexes: list[int]
        :return:
        :rtype:
        """
        for element in self.elements:
            if element.get_field(1).dataInt in lst_indexes:
                self.elements.remove(element)

        for x in range(len(self.elements)):
            var = Variant()
            var.setValue(x)
            self.elements[x].get_field(1).setData(var.toBytes())

    def remove_element(self, int_element_index):
        """
        remove the specified element

        :param int_element_index: the index to remove
        :type int_element_index: int
        :return:
        :rtype:
        """
        for element in self.elements:
            if element.get_field(1).dataInt == int_element_index:
                self.elements.remove(element)
                break
        else:
            return

        for x in range(len(self.elements)):
            var = Variant()
            var.setValue(x)
            self.elements[x].get_field(1).setData(var.toBytes())

    def add_element(self, str_data):
        """
        add a new element

        :param str_data: the data of the element
        :type str_data: str
        :return:
        :rtype:
        """
        var = Variant()
        var.setValue(len(self.elements))
        message = ProtobufMessage()
        message.add_field(EWireType.LengthDelimited, 1, lst_data=var.toBytes())
        message.add_field(EWireType.LengthDelimited, 2, lst_data=str_data)
        self.elements.append(message)
