from .datacollection import DataCollection
from .flattenedarray import FlattenedArray
from .nestedclass import NestedClass
from ..protobuf import ProtobufMessage, EWireType
from .enummember import EnumMember
import time
import struct

PackStruct = struct.Struct('<q')
UnpackStruct = struct.Struct('<Q')


class MechComponentRef(object):

    """
    the save data representation of a mech in the company

    """

    def __init__(self):
        """
        init the object

        """
        self._rawData = None
        self._repackedData = None
        self.mainMessage = ProtobufMessage()
        self.guid = ''
        self.componentName = ''
        self.itemType = EnumMember()
        self.itemLocation = EnumMember()
        self.itemStatus = EnumMember()

    def fromBytes(self, lst_bytes, int_offset=0):
        self._rawData = lst_bytes
        self.mainMessage.fromBytes(lst_bytes, 0)
        self.guid = self.mainMessage.get_field(9).data
        self.componentName = self.mainMessage.get_field(1).data
        self.itemType.fromBytes(self.mainMessage.get_field(7).data)
        self.itemStatus.fromBytes(self.mainMessage.get_field(2).data)
        self.itemLocation.fromBytes(self.mainMessage.get_field(5).data)

    def toBytes(self):

        return self.mainMessage.toBytes()

    @property
    def Type(self):
        return self.itemType.value

    @property
    def Location(self):
        return self.itemLocation.value

    @property
    def Status(self):
        return self.itemStatus.value


    @staticmethod
    def newItem(id, locationId, typeId, refId, prefabName=None, hardpointSlot=None):

        message = ProtobufMessage()
        message.add_field(EWireType.LengthDelimited, 1, lst_data=str(id))

        status = EnumMember()
        status.setType(4254968527)
        status.setValue("Functional")
        message.add_field(EWireType.LengthDelimited, 2, lst_data=status.toBytes())

        if prefabName is not None:
            message.add_field(EWireType.LengthDelimited, 3, lst_data=str(prefabName))

        message.add_field(EWireType.Variant, 4, int_value=1)

        location = EnumMember()
        location.setType(4146553359)
        location.setValue(str(locationId))
        message.add_field(EWireType.LengthDelimited, 5, lst_data=location.toBytes())

        typeenum = EnumMember()
        typeenum.setType(3762464512)
        typeenum.setValue(str(typeId))
        message.add_field(EWireType.LengthDelimited, 7, lst_data=typeenum.toBytes())

        if hardpointSlot is not None:
            if hardpointSlot != 0:
                hardpoint = UnpackStruct.unpack(PackStruct.pack(hardpointSlot))[0]
                message.add_field(EWireType.Variant, 8, int_value=hardpoint)

        message.add_field(EWireType.LengthDelimited, 9, lst_data=refId)

        compRef = MechComponentRef()
        compRef.fromBytes(message.toBytes())
        return compRef