from ..utils import convert_to_stringio, convert_to_bytes, convert_to_string
from ..encoders import encoder_factory, ByteArrayEncoder
from cStringIO import StringIO
from hashlib import md5
from .datacollection import DataCollection
from .flattenedarray import FlattenedArray
from .nestedclass import NestedClass
from .descriptiondef import DescriptionDef
from .locationloaddef import LocationLoadoutDef
from ..protobuf import ProtobufMessage, EWireType
import time

class MechData(object):

    """
    the save data representation of a mech in the company

    """

    def __init__(self):
        """
        init the object

        """
        self._rawData = None
        self._repackedData = None
        self.mainMessage = ProtobufMessage()
        self.guid = ''
        self.nameId = ''
        self.descriptor = DescriptionDef()
        self.descriptorNest = NestedClass(self.descriptor, bol_use_inner=False)
        self.locationArray = FlattenedArray()
        self.locationLoadouts = []
        self.inventoryIds = []


    def fromBytes(self, lst_bytes, int_offset=0):
        self._rawData = lst_bytes
        self.mainMessage.fromBytes(lst_bytes, 0)
        self.guid = self.mainMessage.get_field(9).data
        self.descriptorNest.fromBytes(self.mainMessage.get_field(8).data, 0)
        self.locationArray.fromBytes(self.mainMessage.get_field(1).data, 0)

        self.nameId = '{0} ({1}) - {2}'.format(self.descriptor.name, self.descriptor.uiName, self.guid)

        self.locationLoadouts = []
        for x in range(self.locationArray.size):
            obj_tmp = LocationLoadoutDef()
            obj_tmp.fromBytes(self.locationArray.get_element(x))
            self.locationLoadouts.append(obj_tmp)

    def toBytes(self):

        return self.mainMessage.toBytes()