from ..protobuf import ProtobufMessage, ProtobufField, EWireType


class NestedClass(object):

    def __init__(self, obj_dataclass, bol_use_inner=True, int_field=1):
        self.OuterMessage = ProtobufMessage()
        self.InnerMessage = ProtobufMessage()
        self.dataClass = obj_dataclass
        self.useInner = bol_use_inner
        self.fieldNo = int_field

    def set_ids(self, int_id, int_classId):
        if self.OuterMessage.get_field(1) is None:
            self.OuterMessage.add_field(EWireType.Variant, 1, int_value=int_classId)
        else:
            self.OuterMessage.get_field(1).setValue(int_classId)
        if self.OuterMessage.get_field(3) is None:
            self.OuterMessage.add_field(EWireType.Variant, 3, int_value=int_id)
        else:
            self.OuterMessage.get_field(3).setValue(int_id)

    def fromBytes(self, lst_bytes, int_offset):
        self.OuterMessage.fromBytes(lst_bytes, int_offset)
        if self.useInner:
            self.InnerMessage.fromBytes(self.OuterMessage.get_field(2).data, 0)
            self.dataClass.fromBytes(self.InnerMessage.get_field(self.fieldNo).data, 0)
        else:
            self.dataClass.fromBytes(self.OuterMessage.get_field(2).data, 0)

    def toBytes(self):
        if self.OuterMessage.get_field(2) is None:
            self.OuterMessage.add_field(EWireType.LengthDelimited, 2, lst_data='')
        if self.useInner:
            if self.InnerMessage.get_field(self.fieldNo) is None:
                self.InnerMessage.add_field(EWireType.LengthDelimited, self.fieldNo, lst_data='')
            self.InnerMessage.get_field(self.fieldNo).setData(self.dataClass.toBytes())
            self.OuterMessage.get_field(2).setData(self.InnerMessage.toBytes())
        else:
            self.OuterMessage.get_field(2).setData(self.dataClass.toBytes())

        return self.OuterMessage.toBytes()
