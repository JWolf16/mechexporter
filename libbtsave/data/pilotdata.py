from ..utils import convert_to_stringio, convert_to_bytes, convert_to_string
from ..encoders import encoder_factory, ByteArrayEncoder
from cStringIO import StringIO
from hashlib import md5
from .datacollection import DataCollection
from .flattenedarray import FlattenedArray
from .nestedclass import NestedClass
from ..protobuf import ProtobufMessage, EWireType
import time

class PilotData(object):

    """
    the save data representation of a pilot in the company

    """

    WeaponSkillTrait = 'TraitDefWeaponHit'
    MeleeSkillTrait = 'TraitDefMeleeHit'

    MultiShotAbility = 'AbilityDefG5'
    BreachingShotAbility = 'AbilityDefG8'

    SureFootingAbility = 'AbilityDefP5'
    AcePilotAbility = 'AbilityDefP8'

    BulwarkAbility = 'AbilityDefGu5'
    CoolantVentAbility = 'AbilityDefGu8'

    SensorLockAbility = 'AbilityDefT5A'
    MasterTactician = 'AbilityDefT8A'

    _GunneryBase = 3
    _GunneryAdded = 4

    _PilotingBase = 5
    _PilotingAdded = 6

    _GutsBase = 7
    _GutsAdded = 8

    _TacticsBase = 9
    _TacticsAdded = 10

    _Health = 20

    _UnspentXpBase = 11

    def __init__(self, bol_nested=False):
        """
        init the object

        :param bol_nested: True if the class data is nested in an outer message layer, this seems to only be needed for the commander's meta data
        :type bol_nested: bool
        """
        self.isNested = bol_nested
        self._rawData = None
        self._encoders = []
        self._repackedData = None
        self.dataCollection = DataCollection()
        self.pilotOuterMessage = ProtobufMessage()
        self.pilotMessage = ProtobufMessage()
        self.pilotDef = ProtobufMessage()
        self.pilotTraits = FlattenedArray()
        self.firstAbility = ''
        self.secondAbility = ''
        self.masterAbility = ''
        self.traits = []
        self.nameId = ''
        self.Guid = ''
        self.isDead = False
        self._gunMin = 1
        self._pilotMin = 1
        self._gutsMin = 1
        self._tactMin = 1
        self.customPortrait = ''
        self.humanDescriptor = ProtobufMessage()

        self.dataNest = NestedClass(self.dataCollection)
        self.pilotDefNest = NestedClass(self.pilotDef, bol_use_inner=False)
        self.descriptorNest = NestedClass(self.humanDescriptor, bol_use_inner=False)

    def fromBytes(self, lst_bytes, int_offset):
        return self.setData(lst_bytes)

    def setData(self, lst_bytes):

        # Need to figure out this offset as it changes
        try:
            self._rawData = lst_bytes
            # lst_data = convert_to_bytes(self._rawData)
            fl_start = time.time()

            # if we are nested then pull data out of the embedded message otherwise we can process it directly
            if self.isNested:
                self.pilotOuterMessage.fromBytes(lst_bytes, 0)
                self.pilotMessage.fromBytes(self.pilotOuterMessage.get_field(2).data, 0)
            else:
                self.pilotMessage.fromBytes(lst_bytes, 0)
            self.dataNest.fromBytes(self.pilotMessage.get_field(3).data, 0)
            self.pilotDefNest.fromBytes(self.pilotMessage.get_field(8).data, 0)
            self.pilotTraits.fromBytes(self.pilotDef.get_field(16).data, 0)
            for x in range(self.pilotTraits.size):
                if 'AbilityDef' in self.pilotTraits.get_element(x):
                    if '5' in self.pilotTraits.get_element(x):
                        if self.firstAbility == '':
                            self.firstAbility = self.pilotTraits.get_element(x)
                        else:
                            self.secondAbility = self.pilotTraits.get_element(x)
                    else:
                        self.masterAbility = self.pilotTraits.get_element(x)
                else:
                    self.traits.append(self.pilotTraits.get_element(x))
            self.pilotTraits.clear_elements()
            fl_end = time.time()

            self.descriptorNest.fromBytes(self.pilotDef.get_field(2).data, 0)
            self.nameId = unicode('{0}: {1},{2}'.format(self.humanDescriptor.get_field(5).dataStr, self.humanDescriptor.get_field(3).dataStr, self.humanDescriptor.get_field(4).dataStr), encoding='utf-8')
            self.Guid = self.pilotMessage.get_field(10).dataStr

            obj_field = self.humanDescriptor.get_field(10)

            if obj_field is not None:
                self.customPortrait = obj_field.dataStr

            # ToDo: figure out how to tell if a pilot is alive
            # if self.pilotDef.get_field(40) is not None:
            #     print self.pilotDef.get_field(40).value
            #     print self.pilotDef.get_field(40).data
            #     if self.pilotDef.get_field(40).value >= 0:
            #         self.isDead = True
            self._set_skill_mins()
        except Exception as e:
            print str(e)
            raise
        # print 'Decode Time: {0:0.2f}'.format(fl_end - fl_start)
        # print self.pilotMessage.get_field(10).dataStr

    def toBytes(self):
        self.pilotTraits.clear_elements() # remove all traits, so that we dont keep adding on and on and on
        if self.firstAbility != '':
            self.pilotTraits.add_element(self.firstAbility)
        if self.secondAbility != '':
            self.pilotTraits.add_element(self.secondAbility)
        if self.masterAbility != '':
            self.pilotTraits.add_element(self.masterAbility)
        for trait in self.traits:
            self.pilotTraits.add_element(trait)

        self.pilotMessage.get_field(3).setData(self.dataNest.toBytes())
        self.pilotDef.get_field(16).setData(self.pilotTraits.toBytes())
        self.pilotDef.get_field(2).setData(self.descriptorNest.toBytes())
        self.pilotMessage.get_field(8).setData(self.pilotDefNest.toBytes())
        if self.isNested:
            self.pilotOuterMessage.get_field(2).setData(self.pilotMessage.toBytes())
            return self.pilotOuterMessage.toBytes()
        else:
            return self.pilotMessage.toBytes()

    def _set_traits(self):
        """
        figure out what traits the character has based on their skill levels

        :return:
        :rtype:
        """

        # Reset Health and traits
        health = 3
        self.traits = []

        for x in range(self.Gunnery):
            self.traits.append(self.WeaponSkillTrait + '{0}'.format(x + 1))
        for x in range(self.Piloting):
            self.traits.append(self.MeleeSkillTrait + '{0}'.format(x + 1))

        if self.Piloting >= 4:
            self.traits.append('TraitDefUnsteadySet60')
        if self.Piloting >= 6:
            self.traits.append('TraitDefEvasiveChargeAddOne')
        if self.Piloting >= 7:
            self.traits.append('TraitDefSprintIncrease20')
        if self.Piloting >= 9:
            self.traits.append('TraitDefUnsteadySet80')
        if self.Piloting >= 10:
            self.traits.append('TraitDefEvasiveChargeAddTwo')
            self.traits.append('TraitDefDFASelfDmgReduction')

        if self.Guts >= 4:
            self.traits.append('TraitDefHealthAddOne')
            health += 1
        if self.Guts >= 5:
            self.traits.append('TraitDefRefireReduceOne')
        if self.Guts >= 6:
            self.traits.append('TraitDefOverheatAddFifteen')
        if self.Guts >= 7:
            self.traits.append('TraitDefHealthAddTwo')
            health += 1
        if self.Guts >= 8:
            self.traits.append('TraitDefRefireReduceTwo')
        if self.Guts >= 9:
            self.traits.append('TraitDefOverheatAddThirty')
        if self.Guts >= 10:
            self.traits.append('TraitDefHealthAddThree')
            health += 1

        if self.Tactics >= 4:
            self.traits.append('TraitDefIndirectReduceOne')
        if self.Tactics >= 5:
            self.traits.append('TraitDefMinRangeReduce45')
        if self.Tactics >= 6:
            self.traits.append('TraitDefCalledShotImprove')
        if self.Tactics >= 7:
            self.traits.append('TraitDefIndirectReduceTwo')
        if self.Tactics >= 8:
            self.traits.append('TraitDefMinRangeReduce90')
        if self.Tactics >= 9:
            self.traits.append('TraitDefCalledShotMaster')
        if self.Tactics >= 10:
            self.traits.append('TraitDefIndirectReduceThree')

        # self.dataCollection.findRecord('Health').currentValue.setValue(health)
        # self.pilotDef.get_field(self._Health).setValue(health)

    def _set_skill_mins(self):
        """
        set the minimal skill level needed for specific abilities

        :return:
        :rtype:
        """
        self._gunMin = 1
        self._pilotMin = 1
        self._gutsMin = 1
        self._tactMin = 1
        if self.firstAbility != '':
            if 'G5' in self.firstAbility:
                self._gunMin = 5
            if 'P5' in self.firstAbility:
                self._pilotMin = 5
            if 'Gu5' in self.firstAbility:
                self._gutsMin = 5
            if 'T5' in self.firstAbility:
                self._tactMin = 5

            if self.secondAbility != '':
                if 'G5' in self.secondAbility:
                    self._gunMin = 5
                if 'P5' in self.secondAbility:
                    self._pilotMin = 5
                if 'Gu5' in self.secondAbility:
                    self._gutsMin = 5
                if 'T5' in self.secondAbility:
                    self._tactMin = 5

            if self.masterAbility != '':
                if 'G8' in self.masterAbility:
                    self._gunMin = 8
                if 'P8' in self.masterAbility:
                    self._pilotMin = 8
                if 'Gu8' in self.masterAbility:
                    self._gutsMin = 8
                if 'T8' in self.masterAbility:
                    self._tactMin = 8

    def _set_skill(self, str_attr):
        """
        set a skill to the minimum needed for an ability

        :param str_attr: the last bit of the ability's Id
        :type str_attr: str
        :return:
        :rtype:
        """
        if str_attr == '':
            return
        try:
            str_skill = str_attr[:-1]
            int_level = int(str_attr[-1])
            if str_skill == 'G':
                if self.Gunnery < int_level:
                    self.Gunnery = int_level
            elif str_skill == 'P':
                if self.Piloting < int_level:
                    self.Piloting = int_level
            elif str_skill == 'Gu':
                if self.Guts < int_level:
                    self.Guts = int_level
        except:
            # Tactics end with an A instead of the number like the others
            int_level = int(str_attr[-2])
            if self.Tactics < int_level:
                self.Tactics = int_level

    @property
    def FirstAbility(self):
        return self.firstAbility

    @FirstAbility.setter
    def FirstAbility(self, value):
        self.firstAbility = value
        self._set_skill(value.replace('AbilityDef', ''))
        self._set_skill_mins()

    @property
    def SecondAbility(self):
        return self.secondAbility

    @SecondAbility.setter
    def SecondAbility(self, value):
        self.secondAbility = value
        self._set_skill(value.replace('AbilityDef', ''))
        self._set_skill_mins()

    @property
    def MasterAbility(self):
        return self.masterAbility

    @MasterAbility.setter
    def MasterAbility(self, value):
        self.masterAbility = value
        self._set_skill(value.replace('AbilityDef', ''))
        self._set_skill_mins()

    @property
    def Gunnery(self):
        return self.dataCollection.findRecord('Gunnery').currentValue.value

    @Gunnery.setter
    def Gunnery(self, value):

        # the game keeps the pilots skill as a base when the pilot was created plus the added skills by the user seperate
        # so adjust both as needed
        int_base = self.pilotDef.get_field(self._GunneryBase).value
        if int_base >= value:
            if self.pilotDef.get_field(self._GunneryAdded) is not None:
                self.pilotDef.get_field(self._GunneryAdded).setValue(0)
            self.pilotDef.get_field(self._GunneryBase).setValue(value)
        else:
            if self.pilotDef.get_field(self._GunneryAdded) is not None:
                self.pilotDef.get_field(self._GunneryAdded).setValue(value - int_base)
            else:
                self.pilotDef.add_field(EWireType.Variant, self._GunneryAdded, int_value=(value - int_base))
        self.dataCollection.findRecord('Gunnery').currentValue.setValue(value)
        self._set_traits()


    @property
    def Piloting(self):
        return self.dataCollection.findRecord('Piloting').currentValue.value

    @Piloting.setter
    def Piloting(self, value):
        int_base = self.pilotDef.get_field(self._PilotingBase).value
        if int_base >= value:
            if self.pilotDef.get_field(self._PilotingAdded) is not None:
                self.pilotDef.get_field(self._PilotingAdded).setValue(0)
            self.pilotDef.get_field(self._PilotingBase).setValue(value)
        else:
            if self.pilotDef.get_field(self._PilotingAdded) is not None:
                self.pilotDef.get_field(self._PilotingAdded).setValue(value - int_base)
            else:
                self.pilotDef.add_field(EWireType.Variant, self._PilotingAdded, int_value=(value - int_base))
        self.dataCollection.findRecord('Piloting').currentValue.setValue(value)
        self._set_traits()

    @property
    def Guts(self):
        return self.dataCollection.findRecord('Guts').currentValue.value

    @Guts.setter
    def Guts(self, value):
        int_base = self.pilotDef.get_field(self._GutsBase).value
        if int_base >= value:
            if self.pilotDef.get_field(self._GutsAdded) is not None:
                self.pilotDef.get_field(self._GutsAdded).setValue(0)
            self.pilotDef.get_field(self._GutsBase).setValue(value)
        else:
            if self.pilotDef.get_field(self._GutsAdded) is not None:
                self.pilotDef.get_field(self._GutsAdded).setValue(value - int_base)
            else:
                self.pilotDef.add_field(EWireType.Variant, self._GutsAdded, int_value=(value - int_base))
        self.dataCollection.findRecord('Guts').currentValue.setValue(value)
        self._set_traits()

    @property
    def Tactics(self):
        return self.dataCollection.findRecord('Tactics').currentValue.value

    @Tactics.setter
    def Tactics(self, value):
        int_base = self.pilotDef.get_field(self._TacticsBase).value
        if int_base >= value:
            if self.pilotDef.get_field(self._TacticsAdded) is not None:
                self.pilotDef.get_field(self._TacticsAdded).setValue(0)
            self.pilotDef.get_field(self._TacticsBase).setValue(value)
        else:
            if self.pilotDef.get_field(self._TacticsAdded) is not None:
                self.pilotDef.get_field(self._TacticsAdded).setValue(value - int_base)
            else:
                self.pilotDef.add_field(EWireType.Variant, self._TacticsAdded, int_value=(value - int_base))
        self.dataCollection.findRecord('Tactics').currentValue.setValue(value)
        self._set_traits()

    @property
    def UnspentXp(self):
        return self.dataCollection.findRecord('ExperienceUnspent').currentValue.value

    @UnspentXp.setter
    def UnspentXp(self, value):
        if self.pilotDef.get_field(self._UnspentXpBase) is not None:
            self.pilotDef.get_field(self._UnspentXpBase).setValue(value)
        else:
            self.pilotDef.add_field(EWireType.Variant, self._UnspentXpBase, int_value=value)
        self.dataCollection.findRecord('ExperienceUnspent').currentValue.setValue(value)

    @property
    def hasCustomPortrait(self):
        return not (self.customPortrait == '')

    def setCustomPortrait(self, str_text):
        obj_field = self.humanDescriptor.get_field(10)
        if obj_field is None:
            self.humanDescriptor.add_field(EWireType.LengthDelimited, 10, lst_data=str_text)
        else:
            self.humanDescriptor.get_field(10).setData(str_text)
        self.customPortrait = str_text
        self.pilotDef.remove_field(24)
        self.pilotDef.remove_field(38)