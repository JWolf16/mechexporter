from .keyvalpair import KeyValPair
from ..encoders import encoder_factory, StringEncoder, IntEncoder
from .reflist import RefList
from .refcontaineritem import RefContainerItem
from .refdictionary import RefDictionary
from ..utils import DataType


class RefContainer(object):

    def __init__(self):
        self.containerId = StringEncoder()
        self.containerCount = IntEncoder()
        self._containers = []
        self.keyValCount = IntEncoder()
        self._keyVals = []
        self.refListCount = IntEncoder()
        self._refLists = []
        self.refDictCount = IntEncoder()
        self._refDicts = []
        self._unuseddata = []
        self.encs = []
        self.pilotRoster = []
        self.containerSize = 0
        self.keyValSize = 0
        self.refListSize = 0
        self.encsSize = 0
        self.refDictSize = 0

    def setData(self, lst_bytes):
        """
        set the data and load it

        :param lst_bytes: a list of bytes in integer form
        :type lst_bytes: str
        :return:
        :rtype:
        """
        int_offset = 0
        int_offset = self.containerId.fromBytes(lst_bytes, int_offset)
        int_offset = self.containerCount.fromBytes(lst_bytes, int_offset)
        for x in range(self.containerCount.value):
            obj_keyval = RefContainerItem()
            int_offset = obj_keyval.fromBytes(lst_bytes, int_offset)
            self._containers.append(obj_keyval)
        int_offset = self.keyValCount.fromBytes(lst_bytes, int_offset)
        for x in range(self.keyValCount.value):
            obj_keyval = RefContainerItem()
            int_offset = obj_keyval.fromBytes(lst_bytes, int_offset)
            self._keyVals.append(obj_keyval)
        int_offset = self.refListCount.fromBytes(lst_bytes, int_offset)
        for x in range(self.refListCount.value):
            obj_keyval = RefList()
            int_offset = obj_keyval.fromBytes(lst_bytes, int_offset)
            self._refLists.append(obj_keyval)
        int_offset = self.refDictCount.fromBytes(lst_bytes, int_offset)
        for x in range(self.refDictCount.value):
            obj_keyval = RefDictionary()
            int_offset = obj_keyval.fromBytes(lst_bytes, int_offset)
            self._refDicts.append(obj_keyval)
        self._unuseddata = lst_bytes[int_offset:]
        idex = 0
        try:
            while idex < len(self._unuseddata):
                result = encoder_factory(self._unuseddata, idex)
                self.encs.append(result[0])
                idex = result[1]
        except Exception as e:
            pass
            # print 'Hit at idex: {0}/{1}, {2}'.format(idex, len(self._unuseddata), str(e))
        rosterindex = self.findRefList('PilotRoster.rootList')[0]
        # calculate the pilot list this way, direct referencing
        # unitentaionally adds the commander and takes up additional beds
        lst_rootlist = self.getRefList(rosterindex).values
        self.pilotRoster = []
        for item in lst_rootlist:
            self.pilotRoster.append(item)
        commanderindex = self.findKeyVal('Commander')[0]
        self.pilotRoster.append(self.getKeyVal(commanderindex).value)


    def toBytes(self):
        self.containerSize = 0
        self.keyValSize = 0
        self.refListSize = 0
        self.refDictSize = 0
        lst_bt = []
        lst_bt.append(self.containerId.toBytes())
        lst_bt.append(self.containerCount.toBytes())
        for container in self._containers:
            if not container.isDeleted:
                data = container.toBytes()
                lst_bt.append(data)
                self.containerSize += len(data)
        lst_bt.append(self.keyValCount.toBytes())
        for keyval in self._keyVals:
            if not keyval.isDeleted:
                data = keyval.toBytes()
                lst_bt.append(data)
                self.keyValSize += len(data)
        lst_bt.append(self.refListCount.toBytes())
        for reflist in self._refLists:
            if not reflist.isDeleted:
                data = reflist.toBytes()
                lst_bt.append(data)
                self.refListSize += len(data)
        lst_bt.append(self.refDictCount.toBytes())
        for refdict in self._refDicts:
            if not refdict.isDeleted:
                data = refdict.toBytes()
                lst_bt.append(data)
                self.refDictSize += len(data)
        lst_bt.append(self._unuseddata)
        return ''.join(lst_bt)


    def findContainers(self, str_key):
        """
        get the index of all containers with a given value in their key

        :param str_key: the substring of the key to search for
        :type str_key: str
        :return: a list of indexs for all containers that had a matching key
        :rtype: list[int]
        """
        lst_ret = []
        for x in range(len(self._containers)):
            if self._containers[x].keyContains(str_key):
                lst_ret.append(x)
        return lst_ret

    def getContainer(self, int_idx):
        """
        get the container at a specific index

        :param int_idx: the index
        :type int_idx: int
        :return: the container
        :rtype: KeyValPair
        """
        if int_idx < len(self._containers):
            return self._containers[int_idx]

    def addContainer(self, str_key, data, datatype=DataType.ByteArray):
        """
        add a new container

        :param str_key: the key
        :type str_key: str
        :param data: the data to add
        :type data: str
        :return: the index that was added
        :rtype: int
        """
        ref = RefContainerItem()
        ref.key = str_key
        ref.setValue(data, datatype)
        idx = self.containerCount.value
        self._containers.append(ref)
        self.containerCount.setValue(idx + 1)
        return idx

    def markContainerForDelete(self, int_idx):
        """
        mark a container for deletion

        :param int_idx: the index
        :type int_idx: int
        :return:
        :rtype:
        """
        if int_idx < len(self._containers):
            container = self._containers[int_idx]
            if not container.isDeleted:
                container.isDeleted = True
                self.containerCount.setValue(self.containerCount.value - 1)

    def findKeyVal(self, str_key):
        """
        get the index of all keyvals with a given value in their key

        :param str_key: the substring of the key to search for
        :type str_key: str
        :return: a list of indexs for all keyvals that had a matching key
        :rtype: list[int]
        """
        lst_ret = []
        for x in range(len(self._keyVals)):
            if self._keyVals[x].keyContains(str_key):
                if not self._keyVals[x].isDeleted:
                    lst_ret.append(x)
        return lst_ret

    def getKeyVal(self, int_idx):
        """
        get the keyval at a specific index

        :param int_idx: the index
        :type int_idx: int
        :return: the keyval
        :rtype: KeyValPair
        """
        if int_idx < len(self._keyVals):
            return self._keyVals[int_idx]

    def addKeyVal(self, str_key, data, datatype=DataType.String):
        """
        add a new keyval

        :param str_key: the key
        :type str_key: str
        :param data: the data to add
        :type data: str
        :return: the index that was added
        :rtype: int
        """
        ref = RefContainerItem()
        ref.key = str_key
        ref.setValue(data, datatype)
        idx = self.keyValCount.value
        self._keyVals.append(ref)
        self.keyValCount.setValue(idx + 1)
        return idx

    def markKeyValForDelete(self, int_idx):
        """
        mark a keyval for deletion

        :param int_idx: the index
        :type int_idx: int
        :return:
        :rtype:
        """
        if int_idx < len(self._keyVals):
            container = self._keyVals[int_idx]
            if not container.isDeleted:
                container.isDeleted = True
                self.keyValCount.setValue(self.keyValCount.value - 1)

    def findRefList(self, str_key):
        """
        get the index of all reflists with a given value in their key

        :param str_key: the substring of the key to search for
        :type str_key: str
        :return: a list of indexs for all reflists that had a matching key
        :rtype: list[int]
        """
        lst_ret = []
        for x in range(len(self._refLists)):
            if self._refLists[x].keyContains(str_key):
                if not self._refLists[x].isDeleted:
                    lst_ret.append(x)
        return lst_ret

    def getRefLists(self):
        return self._refLists

    def getRefList(self, int_idx):
        """
        get the reflist at a specific index

        :param int_idx: the index
        :type int_idx: int
        :return: the reflist
        :rtype: RefList
        """
        if int_idx < len(self._refLists):
            return self._refLists[int_idx]

    def addRefList(self, str_key, data=None):
        """
        add a new refList

        :param str_key: the key
        :type str_key: str
        :param data: the data to add
        :type data: str
        :return: the index that was added
        :rtype: int
        """
        ref = RefList()
        ref.key = str_key
        if data is not None:
            for item in data:
                ref.addValue(item)
        idx = self.refListCount.value
        self._refLists.append(ref)
        self.refListCount.setValue(idx + 1)
        return idx

    def markRefListForDelete(self, int_idx):
        """
        mark a reflist for deletion

        :param int_idx: the index
        :type int_idx: int
        :return:
        :rtype:
        """
        if int_idx < len(self._refLists):
            reflist = self._refLists[int_idx]
            if not reflist.isDeleted:
                reflist.isDeleted = True
                self.refListCount.setValue(self.refListCount.value - 1)

    def findRefDict(self, str_key):
        """
        get the index of all refdict with a given value in their key

        :param str_key: the substring of the key to search for
        :type str_key: str
        :return: a list of indexs for all refdict that had a matching key
        :rtype: list[int]
        """
        lst_ret = []
        for x in range(len(self._refDicts)):
            if self._refDicts[x].keyContains(str_key):
                if not self._refDicts[x].isDeleted:
                    lst_ret.append(x)
        return lst_ret

    def getRefDicts(self):
        return self._refDicts

    def getRefDict(self, int_idx):
        """
        get the refdict at a specific index

        :param int_idx: the index
        :type int_idx: int
        :return: the refdictionary
        :rtype: RefDictionary
        """
        if int_idx < len(self._refDicts):
            return self._refDicts[int_idx]

    def markRefDictForDelete(self, int_idx):
        """
        mark a refdict for deletion

        :param int_idx: the index
        :type int_idx: int
        :return:
        :rtype:
        """
        if int_idx < len(self._refDicts):
            refdict = self._refDicts[int_idx]
            if not refdict.isDeleted:
                refdict.isDeleted = True
                self.refDictCount.setValue(self.refDictCount.value - 1)
