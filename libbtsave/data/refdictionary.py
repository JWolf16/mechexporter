from ..encoders import encoder_factory, StringEncoder, encoder_size_factory, IntEncoder, ByteArrayEncoder, ByteEncoder
from ..utils import DataType


class RefDictionary(object):

    LookupEmpty = 0
    LookupInt = 1
    LookupStr = 2

    def __init__(self):
        self.key = ''
        self.lookupType = ByteEncoder()
        self.items = {}
        self.isDeleted = False

    def fromBytes(self, lst_bytes, int_offset):
        obj_id = StringEncoder()
        int_new_offset = obj_id.fromBytes(lst_bytes, int_offset)
        self.key = obj_id.value
        length = IntEncoder()
        int_new_offset = length.fromBytes(lst_bytes, int_new_offset)
        int_new_offset = self.lookupType.fromBytes(lst_bytes, int_new_offset)
        for x in range(length.value):
            key = encoder_factory(lst_bytes, int_new_offset)
            val = encoder_factory(lst_bytes, key[1])
            self.items[key[0].value] = val[0].value
            int_new_offset = val[1]
        return int_new_offset

    def toBytes(self):
        lst_bt =[]
        obj_enc = StringEncoder()
        obj_enc.setValue(self.key)
        lst_bt.append(obj_enc.toBytes())
        obj_len = IntEncoder()
        obj_len.setValue(len(self.items))
        lst_bt.append(obj_len.toBytes())
        lst_bt.append(self.lookupType.toBytes())
        for item in self.items:
            obj_val = StringEncoder()
            obj_val.setValue(self.items[item])
            obj_key = IntEncoder()
            if int(self.lookupType.value) == self.LookupStr:
                obj_key = StringEncoder()
            obj_key.setValue(item)
            lst_bt.append(obj_key.toBytes())
            lst_bt.append(obj_val.toBytes())
        return ''.join(lst_bt)

    def keyContains(self, str_data):
        """
        check if a key contains a specific value

        :param str_data: the value to look for
        :type str_data: str
        :return: True if the key contains the value, False otherwise
        :rtype: bool
        """
        if str_data in self.key:
            return True
        return False

    def deleteValue(self, value):
        for item in self.items:
            if self.items[item] == value:
                self.deleteKey(item)
                break

    def deleteKey(self, key):
        if key in self.items:
            del self.items[key]
