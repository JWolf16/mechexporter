from ..encoders import encoder_factory, StringEncoder, encoder_size_factory, IntEncoder, ByteArrayEncoder
from ..utils import DataType


class RefList(object):


    def __init__(self):
        self.key = ''
        self._valueList = []
        self.isDeleted = False

    def fromBytes(self, lst_bytes, int_offset):
        obj_id = StringEncoder()
        int_new_offset = obj_id.fromBytes(lst_bytes, int_offset)
        self.key = obj_id.value
        length = IntEncoder()
        int_new_offset = length.fromBytes(lst_bytes, int_new_offset)
        for x in range(length.value):
            result = encoder_factory(lst_bytes, int_new_offset)
            self._valueList.append(result[0].value)
            int_new_offset = result[1]
        return int_new_offset

    def toBytes(self):
        obj_enc = StringEncoder()
        obj_enc.setValue(self.key)
        lst_bt = obj_enc.toBytes()
        obj_len = IntEncoder()
        obj_len.setValue(len(self._valueList))
        lst_bt += obj_len.toBytes()
        for val in self._valueList:
            obj_val = StringEncoder()
            obj_val.setValue(val)
            lst_bt += obj_val.toBytes()
        return lst_bt

    def keyContains(self, str_data):
        """
        check if a key contains a specific value

        :param str_data: the value to look for
        :type str_data: str
        :return: True if the key contains the value, False otherwise
        :rtype: bool
        """
        if str_data in self.key:
            return True
        return False

    @property
    def values(self):
        return self._valueList

    def deleteValue(self, value):
        if value in self._valueList:
            self._valueList.remove(value)

    def addValue(self, value):
        self._valueList.append(value)