from ..utils import convert_to_stringio, convert_to_bytes, convert_to_string
from ..encoders import encoder_factory, ByteArrayEncoder
from cStringIO import StringIO
from hashlib import md5
from .datacollection import DataCollection
from .flattenedarray import FlattenedArray
from ..protobuf import ProtobufMessage, EWireType
import time

class ShopData(object):

    """
    the save data representation of a pilot in the company

    """

    def __init__(self):
        """
        init the object

        :param bol_nested: True if the class data is nested in an outer message layer, this seems to only be needed for the commander's meta data
        :type bol_nested: bool
        """

        self._rawData = None
        self._encoders = []
        self._repackedData = None
        self.shopMessage = ProtobufMessage()
        self.dataCollectionOuter = ProtobufMessage()
        self.dataColectionInner = ProtobufMessage()
        self.defOuter = ProtobufMessage()
        self.pilotDef = ProtobufMessage()
        self.activeInventory = FlattenedArray()
        self.firstAbility = ''
        self.secondAbility = ''
        self.masterAbility = ''
        self.traits = []
        self.nameId = ''
        self.isDead = False
        self._gunMin = 1
        self._pilotMin = 1
        self._gutsMin = 1
        self._tactMin = 1

    def setData(self, lst_bytes):

        # Need to figure out this offset as it changes
        try:
            self._rawData = lst_bytes
            # lst_data = convert_to_bytes(self._rawData)
            fl_start = time.time()

            self.shopMessage.fromBytes(lst_bytes, 0)
            self.activeInventory.fromBytes(self.shopMessage.get_field(10).data, 0)
        except Exception as e:
            print str(e)
            raise
        # print 'Decode Time: {0:0.2f}'.format(fl_end - fl_start)
        # print self.pilotMessage.get_field(10).dataStr

    def toBytes(self):
        pass
        # if self.firstAbility != '':
        #     self.pilotTraits.add_element(self.firstAbility)
        # if self.secondAbility != '':
        #     self.pilotTraits.add_element(self.secondAbility)
        # if self.masterAbility != '':
        #     self.pilotTraits.add_element(self.masterAbility)
        # for trait in self.traits:
        #     self.pilotTraits.add_element(trait)
        #
        # self.dataColectionInner.get_field(1).setData(self.dataCollection.toBytes())
        # self.dataCollectionOuter.get_field(2).setData(self.dataColectionInner.toBytes())
        # self.pilotMessage.get_field(3).setData(self.dataCollectionOuter.toBytes())
        # self.pilotDef.get_field(16).setData(self.pilotTraits.toBytes())
        # self.defOuter.get_field(2).setData(self.pilotDef.toBytes())
        # self.pilotMessage.get_field(8).setData(self.defOuter.toBytes())
        # if self.isNested:
        #     self.pilotOuterMessage.get_field(2).setData(self.pilotMessage.toBytes())
        #     return self.pilotOuterMessage.toBytes()
        # else:
        #     return self.pilotMessage.toBytes()