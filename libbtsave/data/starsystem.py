from ..protobuf import ProtobufMessage
from .datacollection import DataCollection
from .flattenedarray import FlattenedArray
from .nestedclass import NestedClass

class StarSystem(object):

    def __init__(self):
        self._rawData = None
        self.systemMessage = ProtobufMessage()
        self.Guid = ''
        self.Uid = ''
        self.Name = ''
        self.systemId = ''
        self.dataCollection = DataCollection()
        self.tags = FlattenedArray()

        self.dataNest = NestedClass(self.dataCollection)
        self.tagsNest = NestedClass(self.tags)

    def fromBytes(self, lst_bytes):
        # Need to figure out this offset as it changes
        try:
            self._rawData = lst_bytes

            self.systemMessage.fromBytes(lst_bytes, 0)
            self.Guid = self.systemMessage.get_field(14).data
            self.Name = self.systemMessage.get_field(17).data
            self.systemId = self.systemMessage.get_field(18).data

            self.dataNest.fromBytes(self.systemMessage.get_field(6).data, 0)
            self.tagsNest.fromBytes(self.systemMessage.get_field(7).data, 0)

        except Exception as e:
            print str(e)
            raise
        # print 'Decode Time: {0:0.2f}'.format(fl_end - fl_start)
        # print self.pilotMessage.get_field(10).dataStr

    def toBytes(self):

        self.systemMessage.get_field(7).setData(self.tagsNest.toBytes())
        self.systemMessage.get_field(6).setData(self.dataNest.toBytes())

        return self.systemMessage.toBytes()
