from ..encoders import encoder_factory, IntEncoder, StringEncoder
from .keyvalpair import KeyValPair


class TransactionLog(object):

    """
    Transaction logs in the data collection system

    """

    def __init__(self):
        self.uid = IntEncoder()
        self.source = StringEncoder()
        self.stack = IntEncoder()
        self.recordName = StringEncoder()
        self.recordId = IntEncoder()
        self.recordValue = KeyValPair()
        self.recordIndex = IntEncoder()
        self.operationtype = StringEncoder()

    def fromBytes(self, lst_bytes, int_offset):
        int_new_offset = self.uid.fromBytes(lst_bytes, int_offset)
        int_new_offset = self.source.fromBytes(lst_bytes, int_new_offset)
        int_new_offset = self.stack.fromBytes(lst_bytes, int_new_offset)
        int_new_offset = self.recordName.fromBytes(lst_bytes, int_new_offset)
        int_new_offset = self.recordId.fromBytes(lst_bytes, int_new_offset)
        int_new_offset = self.operationtype.fromBytes(lst_bytes, int_new_offset)
        int_new_offset = self.recordValue.fromBytes(lst_bytes, int_new_offset)
        int_new_offset = self.recordIndex.fromBytes(lst_bytes, int_new_offset)
        return int_new_offset

    def toBytes(self):
        lst_data = self.uid.toBytes()
        lst_data += self.source.toBytes()
        lst_data += self.stack.toBytes()
        lst_data += self.recordName.toBytes()
        lst_data += self.recordId.toBytes()
        lst_data += self.operationtype.toBytes()
        lst_data += self.recordValue.toBytes()
        lst_data += self.recordIndex.toBytes()
        return lst_data

    def calcSize(self, lst_bytes, int_offset):
        int_new_offset = self.uid.calcSize(lst_bytes, int_offset)
        int_new_offset = self.source.calcSize(lst_bytes, int_new_offset)
        int_new_offset = self.stack.calcSize(lst_bytes, int_new_offset)
        int_new_offset = self.recordName.calcSize(lst_bytes, int_new_offset)
        int_new_offset = self.recordId.calcSize(lst_bytes, int_new_offset)
        int_new_offset = self.operationtype.calcSize(lst_bytes, int_new_offset)
        int_new_offset = self.recordValue.calcSize(lst_bytes, int_new_offset)
        int_new_offset = self.recordIndex.calcSize(lst_bytes, int_new_offset)
        return int_new_offset