
class BaseEncoder(object):

    EncodeType = 0x00

    def __init__(self):
        self._value = None

    def check_encode_type(self, int_type):
        """
        check if encoding type matches this encoder

        :param int_type:
        :type int_type: int
        :return:
        :rtype:
        """

        if int_type != self.EncodeType:
            raise ValueError('Incorrect Data Type {0:02X}, expected: {1:02X}'.format(int_type, self.EncodeType))

    def fromBytes(self, lst_bytes, int_offset):
        """
        decode a value from a byte list

        :param lst_bytes: the byte list
        :type lst_bytes: str
        :param int_offset: the offset to start decoding from
        :type int_offset: int
        :return: the input offset plus the amount of data consumed by this decoder
        :rtype: int
        """
        return int_offset

    def toBytes(self):
        """
        encode this data back to byte form

        :return:
        :rtype: str
        """
        pass

    def calcSize(self, lst_bytes, int_offset):
        """
        calculate the size

        :param lst_bytes: the byte list
        :type lst_bytes: list[int]
        :param int_offset: the offset to start decoding from
        :type int_offset: int
        :return: the input offset plus the amount of data consumed by this decoder
        :rtype: int
        """

        return int_offset

    @property
    def value(self):
        return self._value

    def setValue(self, value):
        """
        set the value of this encoder field

        :param value: the new value
        :type value:
        :return:
        :rtype:
        """
        self._value = value