from .baseencoder import BaseEncoder
from ..utils import DataType


class BooleanEncoder(BaseEncoder):

    EncodeType = DataType.Boolean

    def fromBytes(self, lst_bytes, int_offset):
        self.check_encode_type(ord(lst_bytes[int_offset]))
        self._value = False
        if lst_bytes[int_offset + 1] != '\x00':
            self._value = True
        return int_offset + 2

    def toBytes(self):
        if self._value is None:
            return chr(self.EncodeType) + '\x00'
        if self._value:
            return chr(self.EncodeType) + '\x01'
        return chr(self.EncodeType) + '\x00'

    def calcSize(self, lst_bytes, int_offset):
        return int_offset + 2