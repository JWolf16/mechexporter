from .baseencoder import BaseEncoder
from ..utils import DataType


class ByteEncoder(BaseEncoder):

    EncodeType = DataType.Byte

    def fromBytes(self, lst_bytes, int_offset):
        self.check_encode_type(ord(lst_bytes[int_offset]))
        self._value = ord(lst_bytes[int_offset + 1])
        return int_offset + 2

    def toBytes(self):
        if self._value is None:
            return chr(self.EncodeType) + '\x00'
        return chr(self.EncodeType) + chr(self._value)

    def calcSize(self, lst_bytes, int_offset):
        return int_offset + 2