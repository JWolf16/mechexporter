from .baseencoder import BaseEncoder
from ..utils import DataType, BitField32
import struct

class IntEncoder(BaseEncoder):

    EncodeType = DataType.Int

    def fromBytes(self, lst_bytes, int_offset):

        self.check_encode_type(ord(lst_bytes[int_offset]))
        int_newoffest = int_offset + 5
        self._value = struct.unpack('<i', lst_bytes[int_offset+1:int_offset+5])[0]
        return int_newoffest

    def toBytes(self):
        if self._value is None:
            return chr(self.EncodeType) + struct.pack('<i', 0)

        return chr(self.EncodeType) + struct.pack('<i', self._value)

    def calcSize(self, lst_bytes, int_offset):
        return int_offset + 5