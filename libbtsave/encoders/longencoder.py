from .baseencoder import BaseEncoder
from ..utils import DataType, BitField64
import struct

LongStruct = struct.Struct('<q')

class LongEncoder(BaseEncoder):

    EncodeType = DataType.Long

    def fromBytes(self, lst_bytes, int_offset):
        self.check_encode_type(ord(lst_bytes[int_offset]))
        int_newoffest = int_offset + 9
        self._value = LongStruct.unpack(lst_bytes[int_offset+1:int_offset+9])[0]
        return int_newoffest

    def toBytes(self):
        if self._value is None:
            return chr(self.EncodeType) + LongStruct.pack(0)

        return chr(self.EncodeType) + LongStruct.pack(self._value)

    def calcSize(self, lst_bytes, int_offset):
        return int_offset + 9