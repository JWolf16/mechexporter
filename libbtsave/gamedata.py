from gzip import GzipFile
from .utils import convert_to_stringio, convert_to_bytes, convert_to_string, DataType, string_to_stringio
from .encoders import encoder_factory, ByteArrayEncoder
from cStringIO import StringIO
from hashlib import md5
from .data import DataCollection, PilotData, RefContainer, ShopData, FlattenedArray, StarSystem, NestedClass, Contract, MechData, MechComponentRef
from .protobuf import ProtobufMessage
import time

class GameData(object):
    """
    the main data of the save file format

    """

    RefCounterId = 'BtSaveEdit.RefCounter'

    def __init__(self):
        self._rawData = None
        self.message = ProtobufMessage()
        self._encoders = []
        self._startData = None
        self._endData = None
        self._repackedData = None
        self.starSystemsLoaded = False
        self.Md5 = ''
        self.dataCollection = DataCollection()
        self.simOuterMessage = ProtobufMessage()
        self.simData = ProtobufMessage()
        self.commanderData = PilotData(bol_nested=True)
        self.refContainer = RefContainer()
        self.companyTags = FlattenedArray()
        self.usedMilestones = FlattenedArray()
        self.ownedDlc = FlattenedArray()

        self.dataNest = NestedClass(self.dataCollection)
        self.companyTagsNest = NestedClass(self.companyTags)

        self.pilotMap = {}
        self.nonPlayerPilots = {}
        self.shopMap = {}
        self.starSystemMap = {}
        self.contractMap = {}
        self.mechMap = {}
        self.ComponentMap = {}

        self.loadTimesDebug = {}

    @property
    def hasFlashpointDlc(self):
        if self.ownedDlc.find_element('flashpoint') != -1:
            return True
        return False

    @property
    def hasUrbanWarfareDlc(self):
        if self.ownedDlc.find_element('urbanwarfare') != -1:
            return True
        return False

    def setData(self, lst_bytes):
        """
        set the data and load it

        :param lst_bytes: a list of bytes in integer form
        :type lst_bytes: list[int]
        :return:
        :rtype:
        """
        self.starSystemMap = {}

        try:
            self.loadTimesDebug = {}
            # unzip the data
            st_time = time.time()
            obj_io = string_to_stringio(lst_bytes)
            obj_gzip = GzipFile(fileobj=obj_io, mode='rb')
            self._rawData = obj_gzip.read()
            lst_data = self._rawData
            self.loadTimesDebug['Gzip Load'] = '{0:0.2f}'.format(time.time() - st_time)
            st_time = time.time()

            # now load the uncompressed protobuf message and get the fields we care about
            # ToDo: clean this up a bit by creating some sort of nested message class
            self.message.fromBytes(lst_data, 0)
            self.simOuterMessage.fromBytes(self.message.get_field(4).data, 0)
            self.simData.fromBytes(self.simOuterMessage.get_field(2).data, 0)
            self.loadTimesDebug['Main Data Load'] = '{0:0.2f}'.format(time.time() - st_time)
            st_time = time.time()
            self.dataNest.fromBytes(self.simData.get_field(8).data, 0)
            self.loadTimesDebug['stat collection load'] = '{0:0.2f}'.format(time.time() - st_time)
            st_time = time.time()
            self.commanderData.fromBytes(self.simData.get_field(48).data, 0)
            self.loadTimesDebug['Commander load'] = '{0:0.2f}'.format(time.time() - st_time)
            st_time = time.time()
            self.refContainer.setData(self.message.get_field(6).data)
            self.loadTimesDebug['Ref Container Load'] = '{0:0.2f}'.format(time.time() - st_time)
            st_time = time.time()
            self.companyTagsNest.fromBytes(self.simData.get_field(9).data, 0)
            self.usedMilestones.fromBytes(self.simData.get_field(22).data, 0)
            if self.message.get_field(16) is not None:
                self.ownedDlc.fromBytes(self.message.get_field(16).data, 0)

            st_time = time.time()
            lst_pilot_idx = self.refContainer.findContainers('BattleTech.Pilot,')
            for idx in lst_pilot_idx:
                pilot = PilotData()
                pilot.setData(self.refContainer.getContainer(idx).value)
                if pilot.Guid not in self.refContainer.pilotRoster:
                    pilot.isDead = True
                if not pilot.isDead:    # Only show load pilots who are in the roster to the pilot map
                    self.pilotMap[idx] = pilot
                else:
                    self.nonPlayerPilots[idx] = pilot

            self.loadTimesDebug['Pilot Data Load'] = '{0:0.2f}'.format(time.time() - st_time)
            st_time = time.time()

            lst_shop_idx = self.refContainer.findContainers('BattleTech.Shop,')
            for idx in lst_shop_idx:
                shop = ShopData()
                shop.setData(self.refContainer.getContainer(idx).value)
                self.shopMap[idx] = shop

            self.loadTimesDebug['Shop Data Load'] = '{0:0.2f}'.format(time.time() - st_time)
            st_time = time.time()

            lst_contract_idx = self.refContainer.findContainers('BattleTech.Contract,')
            for idx in lst_contract_idx:
                contract = Contract()
                contract.fromBytes(self.refContainer.getContainer(idx).value)
                self.contractMap[idx] = contract

            self.loadTimesDebug['Contract Load'] = '{0:0.2f}'.format(time.time() - st_time)
            st_time = time.time()

            lst_mech_idx = self.refContainer.findContainers('BattleTech.MechComponentRef,')
            for idx in lst_mech_idx:
                mech = MechComponentRef()
                mech.fromBytes(self.refContainer.getContainer(idx).value)
                self.ComponentMap[idx] = mech

            self.loadTimesDebug['Mech Load'] = '{0:0.2f}'.format(time.time() - st_time)
            st_time = time.time()

            lst_mech_idx = self.refContainer.findContainers('BattleTech.MechDef,')
            for idx in lst_mech_idx:
                mech = MechData()
                mech.fromBytes(self.refContainer.getContainer(idx).value)

                lst_items = []
                lst_ref = self.refContainer.findKeyVal(mech.guid)
                for ref in lst_ref:
                    reflist = self.refContainer.getKeyVal(ref).value
                    for component in self.ComponentMap:
                        item = self.ComponentMap[component]
                        if item.guid in reflist:
                            lst_items.append(component)

                lst_ref = self.refContainer.findRefList(mech.guid + '.Inventory')
                if len(lst_ref) > 0:
                    reflist = self.refContainer.getRefList(lst_ref[0]).values
                    for component in self.ComponentMap:
                        item = self.ComponentMap[component]
                        if item.guid in reflist:
                            lst_items.append(component)
                    mech.inventoryIds = lst_items
                self.mechMap[idx] = mech

            self.loadTimesDebug['Mech Load'] = '{0:0.2f}'.format(time.time() - st_time)
            st_time = time.time()

            # int_offset = 0
            # lst_data = self.message.get_field(6).data
            # m_file = open('game.bin', 'wb')
            # m_file.write(convert_to_string(lst_data))
            # m_file.close()
            # try:
            #     while int_offset < len(lst_data):
            #         lst_result = encoder_factory(lst_data, int_offset)
            #         self._encoders.append(lst_result[0])
            #         int_offset = lst_result[1]
            # except:
            #     pass
            fl_end = time.time()

        except Exception as e:
            print str(e)
            raise
        # print 'Decode Time: {0:0.2f}'.format(fl_end - fl_start)

    def reload_mechs(self):
        self.ComponentMap = {}
        self.mechMap = {}
        st_time = time.time()
        lst_mech_idx = self.refContainer.findContainers('BattleTech.MechComponentRef,')
        for idx in lst_mech_idx:
            mech = MechComponentRef()
            mech.fromBytes(self.refContainer.getContainer(idx).value)
            self.ComponentMap[idx] = mech

        self.loadTimesDebug['Mech Load'] = '{0:0.2f}'.format(time.time() - st_time)
        st_time = time.time()

        lst_mech_idx = self.refContainer.findContainers('BattleTech.MechDef,')
        for idx in lst_mech_idx:
            mech = MechData()
            mech.fromBytes(self.refContainer.getContainer(idx).value)

            lst_items = []
            lst_ref = self.refContainer.findKeyVal(mech.guid)
            for ref in lst_ref:
                reflist = self.refContainer.getKeyVal(ref).value
                for component in self.ComponentMap:
                    item = self.ComponentMap[component]
                    if item.guid in reflist:
                        lst_items.append(component)

            lst_ref = self.refContainer.findRefList(mech.guid + '.Inventory')
            if len(lst_ref) > 0:
                reflist = self.refContainer.getRefList(lst_ref[0]).values
                for component in self.ComponentMap:
                    item = self.ComponentMap[component]
                    if item.guid in reflist:
                        lst_items.append(component)
                mech.inventoryIds = lst_items
            self.mechMap[idx] = mech

        self.loadTimesDebug['Mech Load'] = '{0:0.2f}'.format(time.time() - st_time)
        st_time = time.time()

    def load_star_systems(self):

        self.loadTimesDebug = {}
        st_time = time.time()
        lst_starsystems_idx = self.refContainer.findContainers('BattleTech.StarSystem,')
        # print len(lst_starsystems_idx)
        for idx in lst_starsystems_idx:
            system = StarSystem()
            system.fromBytes(self.refContainer.getContainer(idx).value)
            self.starSystemMap[idx] = system
            # print system.Guid
        self.starSystemsLoaded = True
        self.loadTimesDebug['Star System Load'] = '{0:0.2f}'.format(time.time() - st_time)

    @property
    def value(self):
        return self._rawData

    @property
    def hexStr(self):
        """
        get the value of the bit field

        :return: a hex string
        :rtype: str
        """
        return ''.join('{0:02X}'.format(ord(c)) for c in self._rawData)

    @property
    def size(self):
        return len(self._rawData)

    @property
    def encoderSize(self):
        return len(self._encoders)

    def get_encoder(self, idex):
        return self._encoders[idex]

    def _repack(self):
        """
        repackage the modified data

        :return:
        :rtype:
        """
        self.loadTimesDebug = {}
        st_time = time.time()
        # repack our modified pilots
        for idx in self.pilotMap:
            self.refContainer.getContainer(idx).setValue(self.pilotMap[idx].toBytes(), DataType.ByteArray)
        for idx in self.nonPlayerPilots:
            self.refContainer.getContainer(idx).setValue(self.nonPlayerPilots[idx].toBytes(), DataType.ByteArray)
        self.loadTimesDebug['Pilot Save'] = '{0:0.2f}'.format(time.time() - st_time)
        st_time = time.time()

        for idx in self.starSystemMap:
            self.refContainer.getContainer(idx).setValue(self.starSystemMap[idx].toBytes(), DataType.ByteArray)
        self.loadTimesDebug['Star System Save'] = '{0:0.2f}'.format(time.time() - st_time)
        st_time = time.time()

        for idx in self.contractMap:
            self.refContainer.getContainer(idx).setValue(self.contractMap[idx].toBytes(), DataType.ByteArray)
        self.loadTimesDebug['Contract Save'] = '{0:0.2f}'.format(time.time() - st_time)
        st_time = time.time()

        self.simData.get_field(8).setData(self.dataNest.toBytes())
        self.loadTimesDebug['stat collection Save'] = '{0:0.2f}'.format(time.time() - st_time)
        st_time = time.time()
        self.simData.get_field(48).setData(self.commanderData.toBytes())
        self.simData.get_field(9).setData(self.companyTagsNest.toBytes())
        self.simData.get_field(22).setData(self.usedMilestones.toBytes())
        self.simOuterMessage.get_field(2).setData(self.simData.toBytes())
        self.message.get_field(4).setData(self.simOuterMessage.toBytes())
        self.loadTimesDebug['commander & tags Save'] = '{0:0.2f}'.format(time.time() - st_time)
        st_time = time.time()
        self.message.get_field(6).setData(self.refContainer.toBytes())
        self.loadTimesDebug['Ref Container Save'] = '{0:0.2f}'.format(time.time() - st_time)
        st_time = time.time()
        self._repackedData = self.message.toBytes()
        self.loadTimesDebug['Message Repack Save'] = '{0:0.2f}'.format(time.time() - st_time)
        st_time = time.time()

    @property
    def modifiedData(self):
        if self._repackedData is None:
            self._repack()
        return self._repackedData

    def force_repack(self):
        self._repackedData = None

    @property
    def repackedData(self):
        """
        compress the modified data into a gzip format, but calculate the MD5 hash of the data first
        for later use

        :return: a byte string, the recompressed save data
        :rtype: str
        """
        if self._repackedData is None:
            self._repack()
        obj_io = StringIO()
        obj_zip = GzipFile(fileobj=obj_io, mode='wb')
        obj_zip.write(self._repackedData)
        obj_zip.close()
        obj_io.seek(0)
        str_val = obj_io.getvalue()
        obj_md5 = md5(str_val)
        self.Md5 = obj_md5.digest()
        obj_btary = ByteArrayEncoder()
        obj_btary.setValue(str_val)
        return obj_btary.toBytes()

    @property
    def pilotNames(self):
        """
        get a list of pilot names in the save

        :return:
        :rtype: list[str]
        """
        lst_ret = []
        for idx in self.pilotMap:
            lst_ret.append(self.pilotMap[idx].nameId)
        return lst_ret

    def get_pilot(self, str_name):
        """
        get a specific pilot

        :return:
        :rtype: PilotData
        """
        for idx in self.pilotMap:
            if self.pilotMap[idx].nameId == str_name:
                return self.pilotMap[idx]
        return None

    @property
    def mechNames(self):
        """
        get a list of mech names in the save

        :return:
        :rtype: list[str]
        """
        lst_ret = []
        for idx in self.mechMap:
            lst_ret.append(self.mechMap[idx].nameId)
        return lst_ret

    def get_mech(self, str_name):
        """
        get a specific mech

        :return:
        :rtype: MechData
        """
        for idx in self.mechMap:
            if self.mechMap[idx].nameId == str_name:
                return self.mechMap[idx]
        return None

    def delete_starsystem(self, int_idx):
        if int_idx in self.starSystemMap:
            # Mark the container to not be re-serialized
            self.refContainer.markContainerForDelete(int_idx)

            # do the same for all reflists that refer to the container
            lst_refs = self.refContainer.findRefList(self.starSystemMap[int_idx].Guid)
            for item in lst_refs:
                self.refContainer.markRefListForDelete(item)

            # remove the GUID from the star system list
            lst_refs = self.refContainer.findRefList('StarSystems')
            ref = self.refContainer.getRefList(lst_refs[0])
            ref.deleteValue(self.starSystemMap[int_idx].Guid)

            # remove the system from the mapping
            del self.starSystemMap[int_idx]

    def delete_contract(self, int_idx):
        if int_idx in self.contractMap:
            guid = self.contractMap[int_idx].guid
            # Mark the container to not be re-serialized
            self.refContainer.markContainerForDelete(int_idx)

            # do the same for all reflists that refer to the container
            lst_refs = self.refContainer.findRefList(self.contractMap[int_idx].guid)
            for item in lst_refs:
                self.refContainer.markRefListForDelete(item)

            # remove the GUID from the star system list
            for ref in self.refContainer.getRefLists():
                # ref = self.refContainer.getRefList(lst_refs[0])
                ref.deleteValue(guid)

            # remove the system from the mapping
            del self.contractMap[int_idx]

    def delete_mech_component(self, int_idx):
        if int_idx in self.ComponentMap:
            self.refContainer.markContainerForDelete(int_idx)
            del self.ComponentMap[int_idx]

    def delete_mech(self, int_idx):
        if int_idx in self.mechMap:
            guid = self.mechMap[int_idx].guid

            # delete inventory
            for item in self.mechMap[int_idx].inventoryIds:
                self.delete_mech_component(item)

            # delete references to the mech
            for item in self.refContainer.findKeyVal(guid):
                self.refContainer.markKeyValForDelete(item)

            # delete inventory holder
            for item in self.refContainer.findRefList(guid + '.Inventory'):
                self.refContainer.markRefListForDelete(item)

            # remove the item from the mechbay if applicable
            mechbayidx = self.refContainer.findRefDict('ActiveMechs')
            mechbay = self.refContainer.getRefDict(mechbayidx[0])
            mechbay.deleteValue(guid)

            # finally delete the mech itself
            self.refContainer.markContainerForDelete(int_idx)
            del self.mechMap[int_idx]

    def cleanup_save_data(self):
        """
        run a cleanup of the save data to reduce un-needed bloat

        :return:
        :rtype:
        """
        if not self.starSystemsLoaded:
            self.load_star_systems()
        # Stats collections (called DataCollections here) build up transaction logs over time that seem to be useless
        # other than perhaps to help the developers, so remove them this can greatly reduce bloat
        self.dataCollection.deleteTransactionLogs()
        for starsystem in self.starSystemMap:
            system = self.starSystemMap[starsystem]
            system.dataCollection.deleteTransactionLogs()
        for idx in self.pilotMap:
            pilot = self.pilotMap[idx]
            pilot.dataCollection.deleteTransactionLogs()
        for idx in self.nonPlayerPilots:
            pilot = self.nonPlayerPilots[idx]
            pilot.dataCollection.deleteTransactionLogs()

    def generate_refId(self):
        int_id = 1
        record = self.dataCollection.findRecord(self.RefCounterId)
        if record is not None:
            int_id = record.currentValue.value
        self.dataCollection.updateRecordInt(self.RefCounterId, int_id + 1)
        return 'BtseRef_{0}'.format(int_id)
