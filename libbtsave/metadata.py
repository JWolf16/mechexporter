from gzip import GzipFile
from .utils import convert_to_stringio, convert_to_bytes, convert_to_string, string_to_stringio
from .encoders import encoder_factory, ByteArrayEncoder
from hashlib import md5

class MetaData(object):

    # ToDo: Figure out if we actually ever need to modify this data

    """
    game meta data, used I think just for the load page

    """

    def __init__(self):
        self._rawData = None
        self._decompressed = None
        self._encoders = []
        self.Md5 = ''
        self.Md5Hex = ''

    def setData(self, lst_bytes):

        self._rawData = lst_bytes
        obj_io = string_to_stringio(lst_bytes)
        obj_gzip = GzipFile(fileobj=obj_io, mode='rb')
        self._decompressed = obj_gzip.read()
        self._encoders = []
        int_offset = 0
        lst_data = convert_to_bytes(self._decompressed)
        # ToDo: This fails miserably, as the data is a protobuf message, fix this if we determine a need
        # to decode or modify the meta data
        try:
            while int_offset < self.size:
                lst_result = encoder_factory(lst_data, int_offset)
                self._encoders.append(lst_result[0])
                int_offset = lst_result[1]
        except Exception as e:
            # print str(e)
            pass
    @property
    def value(self):
        return self._decompressed

    @property
    def repackedData(self):
        """
        get the recompressed meta data

        :return: the metadata as a byte string
        :rtype: str
        """
        # for now just repack the raw data since we are not modifying the meta data
        obj_md5 = md5(self._rawData)
        self.Md5 = obj_md5.digest()
        self.Md5Hex = obj_md5.hexdigest()
        obj_btary = ByteArrayEncoder()
        obj_btary.setValue(self._rawData)
        return obj_btary.toBytes()

    @property
    def repackedData2(self):
        obj_btary = ByteArrayEncoder()
        obj_btary.setValue(self._rawData)
        return obj_btary.toBytes()

    @property
    def hexStr(self):
        """
        get the value of the bit field

        :return: a hex string
        :rtype: str
        """
        return ''.join('{0:02X}'.format(ord(c)) for c in self._decompressed)

    @property
    def size(self):
        return len(self._decompressed)

    @property
    def dataReport(self):
        str_enc = ''
        for encoder in self._encoders:
            str_enc += '{0}\n'.format(encoder.value)
        return str_enc