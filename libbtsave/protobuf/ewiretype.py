
class EWireType(object):
    """
    Enum of possible protobuf message field types

    ..note::

        Values for 3 & 4 do exist but are deprecated, thus far the save only seems to make use of variants and length
        delimited types

    """

    Variant = 0
    Fixed64 = 1
    LengthDelimited = 2
    Fixed32 = 5