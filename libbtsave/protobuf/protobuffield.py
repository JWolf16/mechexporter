from .ewiretype import EWireType
from .variant import Variant
from ..utils import convert_to_string

class ProtobufField(object):
    """

    a field in a protobuf message
    """

    def __init__(self):
        self.fieldTag = Variant()
        self.variant = Variant()
        self._otherData = []
        self._rawData = []
        self._modified = True
        self.type = 0
        self.fieldNumber = 0

    def setFieldTag(self, int_type, int_field):
        """
        set the field and the type

        :param int_type: the wiretype of this field
        :type int_type: int
        :param int_field: the field number of the field
        :type int_field: int
        :return:
        :rtype:
        """

        int_new_val = (int_field << 3) | (int_type & 0x07)
        self.type = int_type
        self.fieldNumber = int_field
        self.fieldTag.setValue(int_new_val)


    def fromBytes(self, lst_bytes, int_offset, bol_verb=False):
        """
        decode a value from a byte list

        :param lst_bytes: the byte list
        :type lst_bytes: str
        :param int_offset: the offset to start decoding from
        :type int_offset: int
        :return: the input offset plus the amount of data consumed by this decoder
        :rtype: int
        """
        self._modified = False
        int_start = int_offset
        int_new_offset = self.fieldTag.fromBytes(lst_bytes, int_offset)
        int_fixed_offset = int_new_offset
        int_new_offset = self.variant.fromBytes(lst_bytes, int_new_offset)
        self.type = self.fieldTag.field.get_bit_struct(0, 3).Value
        self.fieldNumber = self.fieldTag.field.get_bit_struct(3, self.fieldTag.field.fieldSize - 3).Value
        if bol_verb:
            print 'wiretype: {0}, field: {1}, FieldTag: {2}'.format(self.type, self.fieldNumber, self.fieldTag.field.Value)
        if self.type == EWireType.Variant:
            pass
        elif self.type == EWireType.LengthDelimited:
            int_end = int_new_offset + self.variant.value
            self._otherData = lst_bytes[int_new_offset:int_end]
            int_new_offset = int_end
        elif self.type == EWireType.Fixed32:
            self._otherData = lst_bytes[int_fixed_offset:int_fixed_offset+4]
            int_new_offset = int_fixed_offset+4
        else:
            raise ValueError('No such wiretype: {0}, field: {1}, FieldTag: {2}'.format(self.type, self.fieldNumber, self.fieldTag.field.Value))
        self._rawData = lst_bytes[int_start:int_new_offset]

        return int_new_offset

    def toBytes(self):
        """
        encode this data back to byte form

        :return:
        :rtype: str
        """
        if self._modified:
            lst_data = [self.fieldTag.toBytes()]
            if self.type not in [EWireType.Fixed32]:
                lst_data.append(self.variant.toBytes())
            if self.type in [EWireType.LengthDelimited, EWireType.Fixed32]:
                lst_data.append(self._otherData)
            self._rawData = ''.join(lst_data)
            self._modified = False
        return self._rawData

    @property
    def value(self):
        return self.variant.value

    @property
    def data(self):
        return self._otherData

    @property
    def dataInt(self):
        var = Variant()
        var.fromBytes(self._otherData, 0)
        return var.value

    @property
    def dataStr(self):
        return self._otherData

    def setValue(self, int_value):
        """
        set the variant data

        :param int_value: the new value
        :type int_value: int
        :return:
        :rtype:
        """
        self._modified = True
        self.variant.setValue(int_value)

    def setData(self, lst_data):
        """
        set the data field

        :param lst_data: the data as a list of bytes in integer form
        :type lst_data: str
        :return:
        :rtype:
        """
        self._modified = True
        self.variant.setValue(len(lst_data))
        self._otherData = lst_data

    def setDataInt(self, int_value):
        """
        set the data field with an int

        :param int_value: the value to set
        :type int_value: int
        :return:
        :rtype:
        """
        self._modified = True
        var = Variant()
        var.setValue(int_value)
        self._otherData = var.toBytes()