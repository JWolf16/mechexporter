from protobuffield import ProtobufField


class ProtobufMessage(object):

    """
    A protocol buffer message

    """

    def __init__(self):
        self._fields = []
        self._error = -1

    def fromBytes(self, lst_bytes, int_offset, bol_verb=False):
        """
        decode a value from a byte list

        :param lst_bytes: the byte list
        :type lst_bytes: str
        :param int_offset: the offset to start decoding from
        :type int_offset: int
        :return: the input offset plus the amount of data consumed by this decoder
        :rtype: int
        """
        self._fields = []
        int_len = len(lst_bytes)
        int_new_offset = int_offset
        if bol_verb:
            print int_len
        while int_new_offset < int_len:
            try:
                obj_field = ProtobufField()
                int_new_offset = obj_field.fromBytes(lst_bytes, int_new_offset, bol_verb=bol_verb)
                self._fields.append(obj_field)
            except Exception as e:
                self._error = int_new_offset
                print 'Error at: {0:0X}'.format(int_new_offset)
                raise
        return int_new_offset

    def toBytes(self):
        """
        encode this data back to byte form

        :return:
        :rtype: str
        """
        lst_data = []
        for field in self._fields:
            lst_data.append(field.toBytes())
        return ''.join(lst_data)

    @property
    def size(self):
        return len(self._fields)

    @property
    def ErrorByte(self):
        return self._error

    def get_field(self, int_field):
        """
        get the first instance of a desired field

        :param int_field: the field number
        :type int_field: int
        :return: the field if it exists
        :rtype: ProtobufField
        """
        for field in self._fields:
            if field.fieldNumber == int_field:
                return field
        return None

    def get_fields(self, int_field):
        """
        get a list of all fields that match the field id

        :param int_field: the field number
        :type int_field: int
        :return: the fields that match the given field number
        :rtype: list[ProtobufField]
        """

        lst_ret = []
        for field in self._fields:
            if field.fieldNumber == int_field:
                lst_ret.append(field)
        return lst_ret

    def add_field(self, int_tag, int_field, int_value=None, lst_data=None):
        """
        add a new field to the meesage

        :param int_tag: the field's wire type
        :type int_tag: int
        :param int_field: the field number
        :type int_field: int
        :param int_value: the value if it exists
        :type int_value: int
        :param lst_data: the data as a list of bytes
        :type lst_data: str
        :return:
        :rtype:
        """
        obj_field = ProtobufField()
        obj_field.setFieldTag(int_tag, int_field)
        if int_value is not None:
            obj_field.setValue(int_value)
        elif lst_data is not None:
            obj_field.setData(lst_data)
        self._fields.append(obj_field)

    def remove_field(self, int_field):
        obj_field = self.get_field(int_field)
        if obj_field is not None:
            self._fields.remove(obj_field)

    def append_field(self, obj_field):
        """
        add a field to the meesage

        :param obj_field: the field to add
        :type obj_field: ProtobufField
        :return:
        :rtype:
        """
        self._fields.append(obj_field)