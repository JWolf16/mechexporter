from ..utils import BitField, BitField8


class Variant(object):

    """
    a protocol buffer variant

    .. note::

        Variants have no set length when serialized, instead they encode this as part of the data.
        data is carried by bits 0 - 6 of a byte with the MSB indicating if the variant has more data (MSB is set) or
        if this is the final byte of the variant (MSB not set)

    """

    def __init__(self):
        self.field = BitField(7)
        self._size = 1

    @property
    def value(self):
        return self.field.Value

    def _check_and_resize(self, int_val):
        """
        resize the varient's underlying bitfield as needed to fit a value

        :param int_val: the value that needs fitting
        :type int_val: int
        :return:
        :rtype:
        """
        int_fieldsize = self.field.fieldSize
        obj_minfield = BitField(int_fieldsize - 7)
        # check if we need to size up the variant by one byte
        if int_val > self.field.maxValue:
            self.field = BitField(int_fieldsize + 7)
            self._size += 1
            return self._check_and_resize(int_val)
        # check if we can shrink the variant by one byte
        elif int_val < obj_minfield.maxValue:
            self.field = BitField(obj_minfield.fieldSize)
            self._size -= 1
            return self._check_and_resize(int_val)
        return

    def setValue(self, int_value):
        """
        set the value of the variant

        :param int_value: the new value
        :type int_value: int
        :return:
        :rtype:
        """
        self._check_and_resize(int_value)
        self.field.setValue(int_value)

    def fromBytes(self, lst_bytes, int_offset):
        """
        decode a value from a byte list

        :param lst_bytes: the byte list
        :type lst_bytes: str
        :param int_offset: the offset to start decoding from
        :type int_offset: int
        :return: the input offset plus the amount of data consumed by this decoder
        :rtype: int
        """
        bol_more = False
        int_new_offset = int_offset
        self.field = BitField(7)
        int_val = ord(lst_bytes[int_new_offset])
        self.field.setValue(int_val)
        if int_val > 127:
            bol_more = True
        int_new_offset += 1
        while bol_more:
            obj_new = BitField(7)
            int_val = ord(lst_bytes[int_new_offset])
            obj_new.setValue(int_val)
            if int_val <= 127:
                bol_more = False
            int_new_offset += 1
            # self.field.prepend(int_val & 0x7F, 7)
            self.field = BitField.concat(obj_new, self.field)
            self._size += 1

        return int_new_offset

    def toBytes(self):
        """
        encode this data back to byte form

        :return:
        :rtype: str
        """
        lst_data = ''
        obj_new = BitField8()
        for x in range(self._size):
            obj_part = self.field.get_bit_struct(7*x, 7)
            obj_new.setValue(obj_part.Value)
            if x != (self._size -1):
                obj_new.setBit(7)
            lst_data += chr(obj_new.Value)
        return lst_data
