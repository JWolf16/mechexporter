

class DataType(object):
    """
    enum for serialization types used by the battletech save file

    .. note::

        This is not complete, other types exist but I have yet to encounter them

    """

    NotSet = 0x00
    Int = 0x01
    Boolean = 0x03
    Byte = 0x04
    Float = 0x07
    Long = 0x08
    String = 0x0D
    ByteArray = 0x0E