import os.path
from cStringIO import StringIO


def read_raw_file(str_file):
    """
    read a file and convert it to a byte list

    :param str_file: the file to read
    :type str_file: str
    :return: a byte list
    :rtype: list[int]
    """
    lst_bytes = []
    if os.path.isfile(str_file):
        m_file = open(str_file, 'rb')
        str_data = m_file.read()
        m_file.close()
        for char in str_data:
            lst_bytes.append(ord(char))
    return lst_bytes


def read_file(str_file):
    """
    read a file and convert it to a byte list

    :param str_file: the file to read
    :type str_file: str
    :return: a byte list
    :rtype: str
    """
    lst_bytes = ''
    if os.path.isfile(str_file):
        m_file = open(str_file, 'rb')
        str_data = m_file.read()
        m_file.close()
        return str_data
    return lst_bytes


def convert_to_string(lst_bytes):
    """
    convert a byte list to a byte string

    :param lst_bytes:
    :type lst_bytes: list[int]
    :return:
    :rtype: str
    """
    obj_io = StringIO()
    for bt in lst_bytes:
        obj_io.write(chr(bt))
    return obj_io.getvalue()


def string_to_stringio(str_data):
    """
    convert a string into an in memory file handle

    :param str_data:
    :type str_data: str
    :return:
    :rtype: StringIO
    """
    obj_io = StringIO()
    obj_io.write(str_data)
    obj_io.seek(0)
    return obj_io


def convert_to_stringio(lst_bytes):
    """
    convert a byte list to a in memory file handle

    :param lst_bytes:
    :type lst_bytes: list[int]
    :return:
    :rtype: StringIO
    """
    obj_io = StringIO()
    for bt in lst_bytes:
        obj_io.write(chr(bt))
    obj_io.seek(0)
    return obj_io


def convert_to_bytes(str_data):
    """
    convert a byte string to a byte list

    :param str_data:
    :type str_data: str
    :return:
    :rtype: list[int]
    """
    lst_bytes = []
    for char in str_data:
        lst_bytes.append(ord(char))
    return lst_bytes