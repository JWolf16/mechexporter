import json
import json5
import os
import os.path

def readJsonFile(str_path):
    if not os.path.isfile(str_path):
        return None
    data = None
    with open(str_path, 'rb') as f:
        try:
            try:
                data = json.load(f)
            except Exception as e:
                f.seek(0)
                data = json5.load(f)
        except Exception as e:
            data = None
    return data