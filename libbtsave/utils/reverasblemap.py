

class ReversableMap(object):

    def __init__(self):
        self._keyMap = {}
        self._valueMap = {}

    def addItem(self, key, value):
        self._keyMap[key] = value
        self._valueMap[value] = key

    def clearItem(self):
        self._keyMap = {}
        self._valueMap = {}

    def keyLookup(self, key):
        return self._keyMap[key]

    def valueLookUp(self, value):
        return self._valueMap[value]