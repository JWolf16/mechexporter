from .basexml import BaseXml



class SettingsStore(BaseXml):

    XmlType = 'SettingsStore'

    def __init__(self):
        self._path = ''
        self.LastLoadPath = ''
        self.LastScanPath = ''


    def setFilePath(self, str_path):
        self._path = str_path

    @property
    def Path(self):
        return self._path