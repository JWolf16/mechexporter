from PySide.QtCore import *
from PySide.QtGui import *


import sys
import time
import os
import os.path
from mechnameDialog import Ui_Dialog_MechName


class MechNameSetter(QDialog, Ui_Dialog_MechName):

    """
    the reputation tab handler

    """

    def __init__(self, parent=None):
        """
        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(MechNameSetter, self).__init__(parent)
        self.setupUi(self)