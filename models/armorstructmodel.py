from PySide.QtCore import *
from PySide.QtGui import *
import time
import json
import copy

from libbtmechporter import LocationData


class ArmorStructTableModel(QAbstractTableModel):
    """
    inventory table model

    """

    def __init__(self, lst_data, parent=None):
        """

        :param lst_data:
        :type lst_data: list[LocationData]
        :param parent:
        :type parent:
        """
        super(ArmorStructTableModel, self).__init__(parent)
        self.Header = ['Location', 'Max Armor', 'Max Rear Armor', 'Max Internal Structure']
        self.inventoryRecords = lst_data

    def rowCount(self, *args, **kwargs):
        return len(self.inventoryRecords)

    def columnCount(self, *args, **kwargs):
        return len(self.Header)

    def flags(self, index):
        """
        set flags for rows and columns in the table

        :param index: the index of the cell
        :type index:
        :return:
        :rtype:
        """
        if not index.isValid():
            return None
        flags = super(ArmorStructTableModel, self).flags(index)
        if index.column() != 0:
            flags |= Qt.ItemIsEditable
        return flags

    def setData(self, index, value, role):
        if not index.isValid():
            return False
        try:
            int_val = int(value)
            record = self.inventoryRecords[index.row()]
            if index.column() == 1:
                record.MaxArmor = int_val
            elif index.column() == 2:
                if record.canEditRear:
                    record.MaxRear = int_val
                else:
                    return False
            elif index.column() == 3:
                record.MaxStruct = int_val
            return True
        except ValueError:
            return False

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role == Qt.ForegroundRole:
            return QBrush(Qt.white)
        elif role == Qt.BackgroundRole:
            return None
        elif role not in [Qt.DisplayRole, Qt.EditRole]:
            return None
        record = self.inventoryRecords[index.row()]
        if index.column() == 0:
            return record.Location
        elif index.column() == 1:
            return record.MaxArmor
        elif index.column() == 2:
            return record.MaxRear
        elif index.column() == 3:
            return record.MaxStruct

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.Header[col]
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return col