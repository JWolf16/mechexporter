from PySide.QtCore import *
from PySide.QtGui import *
import time
import json
import copy

from libbtmechporter import ChassisDefault


class ChassisDefaultTableModel(QAbstractTableModel):
    """
    inventory table model

    """

    ChassisLocations = ['Head', 'RightTorso', 'RightLeg', 'RightArm', 'LeftLeg', 'LeftTorso', 'LeftArm', 'CenterTorso']

    def __init__(self, lst_data, parent=None):
        super(ChassisDefaultTableModel, self).__init__(parent)
        self.Header = ['Location', 'DefId', 'Category', 'Type']
        self.inventoryRecords = []
        for item in lst_data:
            obj = ChassisDefault()
            obj.fromJson(item)
            self.inventoryRecords.append(obj)

    def getDefaults(self):
        lst_things = []
        for record in self.inventoryRecords:
            lst_things.append(record.toJson())
        return lst_things

    def addRecord(self, newrecord):
        """

        :param newrecord:
        :type newrecord: ChassisDefault
        :rtype:
        """
        self.beginResetModel()
        self.inventoryRecords.append(newrecord)
        self.endResetModel()

    def updateRecord(self, index, newrecord):
        """
        change the inventory data loaded into the model

        :param newrecord:
        :type newrecord: ChassisDefault
        :rtype:
        """
        if not index.isValid():
            return
        self.beginResetModel()
        self.inventoryRecords[index.row()] = newrecord
        self.endResetModel()

    def getRecord(self, index):
        if not index.isValid():
            return None
        return self.inventoryRecords[index.row()]

    def deleteRecord(self, index):
        if not index.isValid():
            return
        self.beginResetModel()
        self.inventoryRecords.pop(index.row())
        self.endResetModel()

    def rowCount(self, *args, **kwargs):
        return len(self.inventoryRecords)

    def columnCount(self, *args, **kwargs):
        return len(self.Header)

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role == Qt.ForegroundRole:
            return QBrush(Qt.white)
        elif role == Qt.BackgroundRole:
            return None
        elif role not in [Qt.DisplayRole, Qt.EditRole]:
            return None
        record = self.inventoryRecords[index.row()]
        if index.column() == 0:
            return record.Location
        elif index.column() == 1:
            return record.Id
        elif index.column() == 2:
            return record.Category
        elif index.column() == 3:
            return record.Type

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.Header[col]
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return col
        return None