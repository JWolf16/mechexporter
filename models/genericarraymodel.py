from PySide.QtCore import *
from PySide.QtGui import *
import time
from libbtsave import FlattenedArray


class GenericArrayModel(QAbstractTableModel):
    """
    inventory table model

    """

    def __init__(self, parent=None):
        super(GenericArrayModel, self).__init__(parent)
        self.Header = ['Current Tags']
        self.tagSet = []
        self.validSet = []
        self.validateTags = False

    def update_table(self, tagset, validateSet):
        """
        change the data loaded into the model

        :param tagset:
        :type tagset: FlattenedArray
        :return:
        :rtype:
        """
        fl_st = time.time()
        self.validateTags = False
        self.tagSet = tagset
        self.validSet = validateSet
        if len(self.validSet) > 0:
            self.validateTags = True
        self.refresh()
        # print 'Table Update Op took {0:02f} seconds'.format(time.time() - fl_st)

    def removeTag(self, index):
        if not index.isValid():
            return None
        self.tagSet.pop(index.row())
        self.refresh()

    def removeTags(self, lst_indexes):
        lst_idxs = []
        for index in lst_indexes:
            if index.isValid():
                lst_idxs.append(self.tagSet[index.row()])
        for item in lst_idxs:
            self.tagSet.remove(item)
        self.refresh()

    def addTag(self, str_data):
        self.tagSet.append(str_data)
        self.refresh()

    def refresh(self):
        """
        refresh the table

        :return:
        :rtype:
        """
        self.beginResetModel()
        self.endResetModel()

    def rowCount(self, *args, **kwargs):
        return len(self.tagSet)

    def columnCount(self, *args, **kwargs):
        return len(self.Header)

    def flags(self, index):
        """
        set flags for rows and columns in the table

        :param index: the index of the cell
        :type index:
        :return:
        :rtype:
        """
        if not index.isValid():
            return None
        flags = super(GenericArrayModel, self).flags(index)
        flags |= Qt.ItemIsEditable
        return flags

    def setData(self, index, value, role):
        if not index.isValid():
            return False
        try:
            self.tagSet[index.row()] = value
            return True
        except ValueError:
            return False

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role == Qt.ForegroundRole:
            return QBrush(Qt.white)
        elif role == Qt.BackgroundRole:
            if self.validateTags:
                if self.tagSet[index.row()] not in self.validSet:
                    return QBrush(Qt.darkYellow)
            return None
        elif role not in [Qt.DisplayRole, Qt.EditRole]:
            return None
        if index.column() == 0:
            return str(self.tagSet[index.row()])
        else:
            return None

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.Header[col]
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return col
        return None