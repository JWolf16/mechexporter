from PySide.QtGui import QSortFilterProxyModel
from PySide.QtCore import Qt


class GenericArrayProxyModel(QSortFilterProxyModel):

    def __init__(self, parent=None):
        QSortFilterProxyModel.__init__(self, parent)


    def lessThan(self, left, right):
        leftData = self.sourceModel().data(left, Qt.ItemDataRole)
        rightData = self.sourceModel().data(right, Qt.ItemDataRole)

        return leftData < rightData

    def filterAcceptsRow(self, sourceRow, sourceParent):
        return True