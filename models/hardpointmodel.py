from PySide.QtCore import *
from PySide.QtGui import *
import time
import json
import copy
from base64 import standard_b64decode, standard_b64encode

from libbtsave import ProtobufMessage, FlattenedArray


class HardPointTableModel(QAbstractTableModel):
    """
    inventory table model

    """

    InventoryEvent = Signal(bool)

    FixedEquipmentPrefix = 'FixedEquipment-'
    ChassisLocations = ['Head', 'RightTorso', 'RightLeg', 'RightArm', 'LeftLeg', 'LeftTorso', 'LeftArm', 'CenterTorso']
    Ballistic = 'Ballistic'
    Energy = 'Energy'
    Missile = 'Missile'
    AntiPersonal = 'AntiPersonnel'
    Omni = 'Omni'
    Mount = 'WeaponMount'

    def __init__(self, bt_data, parent=None):
        super(HardPointTableModel, self).__init__(parent)
        self.Header = ['Location', 'Ballistic', 'Energy', 'Missile', 'AntiPersonnel', 'Omni']
        self.inventoryRecords = []
        self.MechList = bt_data
        self.Remainder = ''
        self.currentIdx = 0
        self.initd = False

    def getMechList(self):
        lst_things = []
        for mech in self.MechList:
            lst_things.append(mech['Description']['Name'])
        return lst_things

    def finialize(self):
        self.writeBack()
        return self.MechList

    def count_hardpoints(self, location):
        b = 0
        e = 0
        m = 0
        a = 0
        o = 0
        for hp in location['Hardpoints']:
            if hp[self.Omni]:
                o += 1
            elif hp[self.Mount] == self.Ballistic:
                b += 1
            elif hp[self.Mount] == self.Energy:
                e += 1
            elif hp[self.Mount] == self.Missile:
                m += 1
            elif hp[self.Mount] == self.AntiPersonal:
                a += 1
        return [location['Location'], b, e, m, a, o]

    def writeBack(self):
        mech = self.MechList[self.currentIdx]
        for loc in self.inventoryRecords:
            for location in mech['Chassis']['Locations']:
                if location['Location'] == loc[0]:
                    lst_hp = []
                    for i in range(1, len(loc)):
                        # print '{0}: {1}'.format(i, loc[i])
                        point = self.Ballistic
                        omni = False
                        if i == 2:
                            point = self.Energy
                        elif i == 3:
                            point = self.Missile
                        elif i == 4:
                            point = self.AntiPersonal
                        elif i == 5:
                            omni = True
                        for x in range(loc[i]):
                            hp = {
                                self.Mount : point,
                                self.Omni : omni
                            }
                            lst_hp.append(hp)
                    location['Hardpoints'] = lst_hp

    def update_table(self, int_idex):
        """
        change the inventory data loaded into the model

        :param dataCollection:
        :type dataCollection: libbtsave.DataCollection
        :return:
        :rtype:
        """
        fl_st = time.time()
        if self.initd:
            self.writeBack()
        else:
            self.initd = True
        self.beginResetModel()
        mech = self.MechList[int_idex]
        self.currentIdx = int_idex
        self.inventoryRecords = []
        for location in mech['Chassis']['Locations']:
            self.inventoryRecords.append(self.count_hardpoints(location))
        self.endResetModel()

    def rowCount(self, *args, **kwargs):
        return len(self.inventoryRecords)

    def columnCount(self, *args, **kwargs):
        return len(self.Header)

    def flags(self, index):
        """
        set flags for rows and columns in the table

        :param index: the index of the cell
        :type index:
        :return:
        :rtype:
        """
        if not index.isValid():
            return None
        flags = super(HardPointTableModel, self).flags(index)
        if index.column() != 0:
            flags |= Qt.ItemIsEditable
        return flags

    def setData(self, index, value, role):
        if not index.isValid():
            return False
        try:
            int_val = int(value)
            self.inventoryRecords[index.row()][index.column()] = int_val
            return True
        except ValueError:
            return False

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role == Qt.ForegroundRole:
            return QBrush(Qt.white)
        elif role == Qt.BackgroundRole:
            return None
        elif role not in [Qt.DisplayRole, Qt.EditRole]:
            return None
        return self.inventoryRecords[index.row()][index.column()]

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.Header[col]
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return col
        return None