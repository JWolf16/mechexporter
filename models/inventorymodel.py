from PySide.QtCore import *
from PySide.QtGui import *
import time
import json
import copy
from collections import OrderedDict

from libbtmechporter import CostCalculator, WikiExporter, LocationData, MECHDEF_TEMPLATE_ORDER, CHASSISDEF_TEMPLATE_ORDER

class InventoryTableModel(QAbstractTableModel):
    """
    inventory table model

    """

    InventoryEvent = Signal(bool)

    FixedEquipmentPrefix = 'FixedEquipment-'
    ChassisLocations = ['Head', 'RightTorso', 'RightLeg', 'RightArm', 'LeftLeg', 'LeftTorso', 'LeftArm', 'CenterTorso']
    Ballistic = 'Ballistic'
    Energy = 'Energy'
    Missile = 'Missile'
    AntiPersonal = 'AntiPersonnel'
    Omni = 'Omni'
    Mount = 'WeaponMount'
    MountID = 'weaponMountID'

    def __init__(self, parent=None):
        super(InventoryTableModel, self).__init__(parent)
        self.Header = ['Mech Name','Mech ID', 'Chassis ID', 'UI Name']
        self.inventoryRecords = []
        self.WikiExporter = None

    def update_table(self, lst_jsons):
        """
        change the inventory data loaded into the model

        :param dataCollection:
        :type dataCollection: libbtsave.DataCollection
        :return:
        :rtype:
        """
        fl_st = time.time()
        self.beginResetModel()
        self.inventoryRecords = lst_jsons
        self.endResetModel()
        # print 'Table Update Op took {0:02f} seconds'.format(time.time() - fl_st)

    def updateRecord(self, str_name, int_value):
        """
        add or update a record

        :param str_name: the record name
        :type str_name: str
        :param int_value: the value
        :type int_value: int
        :return:
        :rtype:
        """
        self.dataCollection.updateRecordInt(str_name, int_value)
        self.refresh()

    def WriteMech(self, index, writePath):
        if not index.isValid():
            return False
        data = copy.deepcopy(self.inventoryRecords[index.row()])
        x = open(writePath, 'wb')
        x.write(json.dumps(data, indent=4))
        x.close()

    def ExportRecords(self, index, writePath, fl_multiplier, rtExport, weaponPrefabs, useWeaponMountID, reorderDefs):
        if not index.isValid():
            return False
        data = copy.deepcopy(self.inventoryRecords[index.row()])
        coster = CostCalculator(data)
        coster.calculateCost(fl_multiplier)
        data['Chassis']['Description']['Cost'] = coster.ChassisDefCost
        data['Description']['Cost'] = coster.MechDefCost
        data['simGameMechPartCost'] = coster.SimGamePartCost

        if rtExport:
            wikiData = WikiExporter(writePath + 'roguetech_wikidef.xlsx')
            wname = data['Chassis']['Description']['Name']
            wvariant = data['Chassis']['VariantName']
            wdesc = data['Chassis']['Description']['Details']
            wton = data['Chassis']['Tonnage']
            wclass = data['Chassis']['weightClass']
            wmdamage = data['Chassis']['MeleeDamage']
            wdfadamage = data['Chassis']['DFADamage']
            wstruct = 0
            wcarmor = 0
            wmarmor = 0
            wjj = data['Chassis']['MaxJumpjets']
            b = 0
            e = 0
            m = 0
            a = 0
            o = 0

            for item in data['Chassis']['Locations']:

                if item['MaxRearArmor'] != -1:
                    wmarmor += item['MaxRearArmor']
                wmarmor += item['MaxArmor']
                wstruct += item['InternalStructure']
                for hp in item['Hardpoints']:
                    if hp[self.Omni]:
                        o += 1
                    elif hp[self.Mount] == self.Ballistic:
                        b += 1
                    elif hp[self.Mount] == self.Energy:
                        e += 1
                    elif hp[self.Mount] == self.Missile:
                        m += 1
                    elif hp[self.Mount] == self.AntiPersonal:
                        a += 1

            for item in data['Locations']:
                wcarmor += item['AssignedArmor']
                wcarmor += item['AssignedRearArmor']

            wikiData.export(wname, wvariant, wstruct, wcarmor, wmarmor, wton, wclass, wmdamage, wdfadamage, wdesc, b, e,
                            m, a, o, wjj)

        # chassis = data['Chassis']
        chassis = OrderedDict()
        if 'Custom' in data['Chassis']:
            chassis['Custom'] = OrderedDict()
        if ('fixedEquipment' in data['Chassis']) or ('FixedEquipment' in data['Chassis']):
            chassis['FixedEquipment'] = []
        for tag in data['Chassis']:
            chassis[tag] = data['Chassis'][tag]
        mech = data
        del mech['Chassis']
        try:
            del mech['Inventory']
        except:
            pass

        for location in self.ChassisLocations:
            if location in chassis:
                del chassis[location]

        try:
            lst_delete = []
            for item in mech['inventory']:
                if 'SimGameUID' in item:
                    if item['SimGameUID'] is not None:
                        if item['SimGameUID'].startswith(self.FixedEquipmentPrefix):
                            lst_delete.append(item)
                if not weaponPrefabs:
                    if 'prefabName' in item:
                        item['prefabName'] = None
                    if 'hasPrefabName' in item:
                        item['hasPrefabName'] = False
                if 'IsFixed' in item:
                    del item['IsFixed']
            for item in lst_delete:
                mech['inventory'].remove(item)
        except Exception as e:
            print e

        extraTags = ['SimGameMechPartCost', 'Name', 'IsDestroyed', 'IsDamaged', 'HasDestroyedComponents', 'MechDefCurrentArmor',
                     'MechDefAssignedArmor', 'MechDefAbsoluteMaxArmor', 'MechDefCurrentStructure', 'MechDefMaxStructure',
                     'BattleValue', 'PaintTextureID']
        for item in extraTags:
            try:
                del mech[item]
            except:
                pass

        extraChassisTags = ['fixedEquipment']

        if 'fixedEquipment' in chassis:
            chassis['FixedEquipment'] = copy.copy(chassis['fixedEquipment'])

        for item in extraChassisTags:
            try:
                del chassis[item]
            except:
                pass

        if 'FixedEquipment' in chassis:
            if chassis['FixedEquipment'] is None:
                chassis['FixedEquipment'] = []
            for item in chassis['FixedEquipment']:
                if 'IsFixed' in item:
                    del item['IsFixed']

        for item in chassis['Locations']:
            for hp in item['Hardpoints']:
                if 'WeaponMountValue' in hp:
                    del hp['WeaponMountValue']
                if self.MountID in hp:
                    if useWeaponMountID:
                        del hp[self.Mount]
                    else:
                        del hp[self.MountID]

        try:
            del mech['MechTags']['IsEmpty']
            del mech['MechTags']['Count']
        except:
            pass

        try:
            del mech['RequiredToSpawnCompanyTags']['IsEmpty']
            del mech['RequiredToSpawnCompanyTags']['Count']
        except:
            pass

        if reorderDefs:
            mech = self.reOrderMechDef(mech)
            chassis = self.reOrderChassisDef(chassis)

        chassisJson = json.dumps(chassis, indent=4)
        mechJson = json.dumps(mech, indent=4)

        try:
            mechfile = open('{0}{1}.json'.format(writePath, mech['Description']['Id']), 'wb')
            mechfile.write(mechJson)
            mechfile.close()

            chassisfile = open('{0}{1}.json'.format(writePath, chassis['Description']['Id']), 'wb')
            chassisfile.write(chassisJson)
            chassisfile.close()
        except:
            return False
        self.refresh()
        return True

    def reOrderMechDef(self, jMech):
        """

        :param jMech:
        :type jMech: OrderedDict
        :return:
        """

        mech = OrderedDict()
        for key in MECHDEF_TEMPLATE_ORDER:
            if key in jMech:
                mech[key] = jMech[key]
        for key in jMech:
            if key not in mech:
                print('Oh No missing key: {0}'.format(key))
        return mech

    def reOrderChassisDef(self, jChassis):
        """

        :param jChassis:
        :type jChassis: OrderedDict
        :return:
        """

        chassis = OrderedDict()
        for key in CHASSISDEF_TEMPLATE_ORDER:
            if key in jChassis:
                chassis[key] = jChassis[key]
        for key in jChassis:
            if key not in chassis:
                print('Oh No Chassis missing key: {0}'.format(key))
        return chassis



    def flags(self, index):
        """
        set flags for rows and columns in the table

        :param index: the index of the cell
        :type index:
        :return:
        :rtype:
        """
        if not index.isValid():
            return None
        return super(InventoryTableModel, self).flags(index)
        # if index.column() not in [1, 2]:
        #     return super(InventoryTableModel, self).flags(index)
        # else:
        #     # make the current value column editable
        #     flags = super(InventoryTableModel, self).flags(index)
        #     flags |= Qt.ItemIsEditable
        #     return flags

    def setData(self, index, value, role):
        if not index.isValid():
            return False
        try:
            record = self.inventoryRecords[index.row()]
            if index.column() == 2:
                record['Chassis']['Description']['Id'] = value
                record['ChassisID'] = value
                self.dataChanged.emit(index, index)
                return True
            elif index.column() == 1:
                record['Description']['Id'] = value
                self.dataChanged.emit(index, index)
                return True
        except ValueError:
            return False

    def refresh(self):
        """
        refresh the table

        :return:
        :rtype:
        """
        self.beginResetModel()
        self.endResetModel()

    def rowCount(self, *args, **kwargs):
        return len(self.inventoryRecords)

    def columnCount(self, *args, **kwargs):
        return len(self.Header)

    def getNameAndModel(self, index):
        if not index.isValid():
            return ['', '']
        record = self.inventoryRecords[index.row()]
        return [str(record['Chassis']['Description']['Name']), str(record['Chassis']['VariantName'])]

    def getDescription(self, index):
        if not index.isValid():
            return ''
        record = self.inventoryRecords[index.row()]
        return record['Description']['Details']

    def setDescription(self, index, str_text):
        if not index.isValid():
            return
        record = self.inventoryRecords[index.row()]
        record['Description']['Details'] = str_text
        record['Chassis']['Description']['Details'] = str_text

    def getYangsThoughts(self, index):
        if not index.isValid():
            return ''
        record = self.inventoryRecords[index.row()]
        return record['Chassis']['YangsThoughts']

    def setYangsThoughts(self, index, str_text):
        if not index.isValid():
            return
        record = self.inventoryRecords[index.row()]
        record['Chassis']['YangsThoughts'] = str_text

    def getStockRole(self, index):
        if not index.isValid():
            return ''
        record = self.inventoryRecords[index.row()]
        if 'StockRole' in record['Chassis']:
            return record['Chassis']['StockRole']
        return ''

    def setStockRole(self, index, str_text):
        if not index.isValid():
            return
        record = self.inventoryRecords[index.row()]
        record['Chassis']['StockRole'] = str_text

    def getTagSet(self, index, bol_chassis):
        if not index.isValid():
            return []
        record = self.inventoryRecords[index.row()]
        if bol_chassis:
            return record['Chassis']['ChassisTags']['items']
        else:
            return record['MechTags']['items']

    def setTagSet(self, index, lst_tags, bol_chassis):
        if not index.isValid():
            return
        record = self.inventoryRecords[index.row()]
        if bol_chassis:
            record['Chassis']['ChassisTags']['items'] = lst_tags
        else:
            record['MechTags']['items'] = lst_tags

    def setNameAndModel(self, index, str_name, str_model):
        if not index.isValid():
            return None
        invalChars = ['<', '>', ':', '"', '/', '\\', '|', '?', '*']
        record = self.inventoryRecords[index.row()]
        record['Description']['Name'] = str_name
        record['Description']['UIName'] = '{0} {1}'.format(str_name, str_model)
        record['Chassis']['Description']['Name'] = str_name
        record['Chassis']['Description']['UIName'] = str_name
        record['Chassis']['VariantName'] = str_model
        idname = '{0}_{1}'.format(''.join(str_name.split()).lower(), str_model)
        for char in invalChars:
            idname = idname.replace(char, '-')
        record['Description']['Id'] = 'mechdef_{0}'.format(idname)
        record['Chassis']['Description']['Id'] = 'chassisdef_{0}'.format(idname)
        record['ChassisID'] = 'chassisdef_{0}'.format(idname)
        self.refresh()

    def ImportCustomBlock(self, index, dct_data):
        if not index.isValid():
            return
        record = self.inventoryRecords[index.row()]
        record['Chassis']['Custom'] = dct_data
        if 'ChassisDefaults' in record['Chassis']['Custom']:
            data = copy.deepcopy(record['Chassis']['Custom']['ChassisDefaults'])
            try:
                record['Chassis']['Custom']['ChassisDefaults'][0]
            except:
                if len(data) > 0:
                    record['Chassis']['Custom']['ChassisDefaults'] = [data]
        self.refresh()

    def getArmActuatorLimits(self, index):
        if not index.isValid():
            return [None, None]
        record = self.inventoryRecords[index.row()]
        lst_ret = [None, None]
        if 'Custom' in record['Chassis']:
            if 'ArmActuatorSupport' in record['Chassis']['Custom']:
                if 'RightLimit' in record['Chassis']['Custom']['ArmActuatorSupport']:
                    lst_ret[0] = record['Chassis']['Custom']['ArmActuatorSupport']['RightLimit']
                if 'LeftLimit' in record['Chassis']['Custom']['ArmActuatorSupport']:
                    lst_ret[1] = record['Chassis']['Custom']['ArmActuatorSupport']['LeftLimit']
        return lst_ret

    def getAssemblyVariant(self, index):
        if not index.isValid():
            return ['', False, True]
        record = self.inventoryRecords[index.row()]
        lst_ret = ['', False, True]
        if 'Custom' in record['Chassis']:
            if 'AssemblyVariant' in record['Chassis']['Custom']:
                if 'PrefabID' in record['Chassis']['Custom']['AssemblyVariant']:
                    lst_ret[0] = record['Chassis']['Custom']['AssemblyVariant']['PrefabID']
                if 'Exclude' in record['Chassis']['Custom']['AssemblyVariant']:
                    lst_ret[1] = record['Chassis']['Custom']['AssemblyVariant']['Exclude']
                if 'Include' in record['Chassis']['Custom']['AssemblyVariant']:
                    lst_ret[2] = record['Chassis']['Custom']['AssemblyVariant']['Include']
        return lst_ret

    def getChassisDefaults(self, index):
        if not index.isValid():
            return []
        record = self.inventoryRecords[index.row()]
        lst_ret = []
        if 'Custom' in record['Chassis']:
            if 'ChassisDefaults' in record['Chassis']['Custom']:
                return record['Chassis']['Custom']['ChassisDefaults']
        return lst_ret

    def setArmActuatorLimits(self, index, right, left):
        if not index.isValid():
            return
        record = self.inventoryRecords[index.row()]
        if right is None and left is None:
            try:
                del record['Chassis']['Custom']['ArmActuatorSupport']
            except:
                pass
            return
        if 'Custom' not in record['Chassis']:
            record['Chassis']['Custom'] = OrderedDict()
        if 'ArmActuatorSupport' not in record['Chassis']['Custom']:
            record['Chassis']['Custom']['ArmActuatorSupport'] = OrderedDict()
        if right is None:
            try:
                del record['Chassis']['Custom']['ArmActuatorSupport']['RightLimit']
            except:
                pass
        else:
            record['Chassis']['Custom']['ArmActuatorSupport']['RightLimit'] = right
        if left is None:
            try:
                del record['Chassis']['Custom']['ArmActuatorSupport']['LeftLimit']
            except:
                pass
        else:
            record['Chassis']['Custom']['ArmActuatorSupport']['LeftLimit'] = left

    def setAssemblyVariant(self, index, prefabid, exclude, include):
        if not index.isValid():
            return
        record = self.inventoryRecords[index.row()]
        if prefabid is None:
            try:
                del record['Chassis']['Custom']['AssemblyVariant']
            except:
                pass
        if 'Custom' not in record['Chassis']:
            record['Chassis']['Custom'] = OrderedDict()
        if 'AssemblyVariant' not in record['Chassis']['Custom']:
            record['Chassis']['Custom']['AssemblyVariant'] = OrderedDict()
        record['Chassis']['Custom']['AssemblyVariant']['PrefabID'] = prefabid
        record['Chassis']['Custom']['AssemblyVariant']['Exclude'] = exclude
        record['Chassis']['Custom']['AssemblyVariant']['Include'] = include

    def setChassisDefaults(self, index, data):
        if not index.isValid():
            return
        record = self.inventoryRecords[index.row()]
        if len(data) == 0:
            try:
                del record['Chassis']['Custom']['ChassisDefaults']
            except:
                pass
        if 'Custom' not in record['Chassis']:
            record['Chassis']['Custom'] = OrderedDict()
        if 'AssemblyVariant' not in record['Chassis']['Custom']:
            record['Chassis']['Custom']['ChassisDefaults'] = []
        record['Chassis']['Custom']['ChassisDefaults'] = data

    def _get_location_records(self, index, loc):
        record = self.inventoryRecords[index.row()]
        lst_locs = []
        for location in record['Locations']:
            if location['Location'] == loc:
                lst_locs.append(location)
                break
        for location in record['Chassis']['Locations']:
            if location['Location'] == loc:
                lst_locs.append(location)
                break
        return lst_locs


    def getArmorData(self, index):
        if not index.isValid():
            return
        record = self.inventoryRecords[index.row()]
        lst_locs = []
        for location in self.ChassisLocations:
            locData = LocationData()
            lst_dat = self._get_location_records(index, location)
            mechRecord = lst_dat[0]
            chassisRecord = lst_dat[1]
            locData.Location = location
            locData.MaxArmor = chassisRecord['MaxArmor']
            locData.MaxRear = chassisRecord['MaxRearArmor']
            locData.MaxStruct = chassisRecord['InternalStructure']
            lst_locs.append(locData)
        return lst_locs

    def setArmorData(self, index, locdata):
        """

        :param index:
        :type index:
        :param locdata:
        :type locdata: list[LocationData]
        :return:
        :rtype:
        """
        if not index.isValid():
            return
        record = self.inventoryRecords[index.row()]
        lst_locs = []
        for location in locdata:
            lst_locs = self._get_location_records(index, location.Location)
            mechRecord = lst_locs[0]
            chassisRecord = lst_locs[1]
            chassisRecord['MaxArmor'] = location.MaxArmor
            if mechRecord['AssignedArmor'] > location.MaxArmor:
                mechRecord['AssignedArmor'] = location.MaxArmor
                mechRecord['CurrentArmor'] = location.MaxArmor
            chassisRecord['MaxRearArmor'] = location.MaxRear
            if location.canEditRear:
                if mechRecord['AssignedRearArmor'] > location.MaxRear:
                    mechRecord['AssignedRearArmor'] = location.MaxRear
                    mechRecord['CurrentRearArmor'] = location.MaxRear
            chassisRecord['InternalStructure'] = location.MaxStruct
            mechRecord['CurrentInternalStructure'] = location.MaxStruct

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role == Qt.ForegroundRole:
            return QBrush(Qt.white)
        # elif role == Qt.BackgroundRole:
        #     return QBrush(Qt.black)
        elif role not in [Qt.DisplayRole, Qt.EditRole]:
            return None
        record = self.inventoryRecords[index.row()]
        if index.column() == 0:
            return str(record['Description']['Name'])
        elif index.column() == 1:
            return str(record['Description']['Id'])
        elif index.column() == 2:
            return str(record['Chassis']['Description']['Id'])
        elif index.column() == 3:
            return str(record['Description']['UIName'])
        else:
            return None

    def deleteRecord(self, index):
        if not index.isValid():
            return None
        self.inventoryRecords.pop(index.row())
        self.refresh()


    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.Header[col]
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return col
        return None